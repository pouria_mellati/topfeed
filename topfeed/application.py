"""
Created on Jan 17, 2013

@author: Pouria Mellati
"""
from gi.repository import Gtk, Gdk, GLib, WebKit, Soup

# If the following two lines are not run before everything else, extremely weird things happen!
GLib.threads_init()
Gdk.threads_init()

from dependency_injection import FeatureBroker, DI
__builtins__.tf_inject = FeatureBroker()

from topfeed.gui.base_widgets import FeedItemsWindow, FeedItemsCategory
from topfeed.gui.themes.rectangular import RectangularFeedWidget
from topfeed.gui.gtk_icon_factory import TopfeedGtkIconFactory
from topfeed.dialogs import LoginDialoge, StartersGuide
from topfeed.domain_classes import Channel, Feed
from topfeed.gui.utils import Color
from topfeed.server import Server, ServerSession, AuthorizationError
from threading import Thread
from datetime import date, datetime
from Queue import Queue
from topfeed.utils import ensure_directory_exists
from settings import RealClientAppSettings
from feeds_fetcher import DevelopmentCachingFeedsFetcher, RealFeedsFetcher
from os import path
import logging, sys, os, shutil

class Controller(object):
    def __init__(self, server_url_http, server_url_https):
        if not server_url_http or not server_url_https:
            raise ValueError("The server url parameters are required.")

        tf_inject.provide(DI.app_name, 'TopFeed')
        tf_inject.provide(DI.app_version, '0.9.2')
        tf_inject.provide(DI.app_home, path.join(path.expanduser('~'), '.' + tf_inject[DI.app_name]))
        tf_inject.provide(DI.app_tmp_dir, path.join(tf_inject[DI.app_home], 'tmp'))
        tf_inject.provide(DI.app_image_cache_dir, self._create_img_cache_dir_for_now_and_cleanup_olds(path.join(tf_inject[DI.app_tmp_dir], 'image_cache')))

        # TODO: Users of dirs should ensure existence them-selves.
        for user_app_dir in [DI.app_home, DI.app_tmp_dir, DI.app_image_cache_dir]:
            ensure_directory_exists(tf_inject[user_app_dir])

        tf_inject.provide(DI.resources_dir, path.join(path.dirname(__file__), '..', 'topfeed-resources'))
        tf_inject.provide(DI.path_to_widget_theme_base_uri, path.join(tf_inject[DI.resources_dir], 'widget_theme_base_uri'))
        tf_inject.provide(DI.path_to_start_script, path.join(path.abspath(path.dirname(__file__)), '..', 'run'))
        tf_inject.provide(DI.FeedsFetcher, RealFeedsFetcher())
        tf_inject.provide(DI.ui_descriptions_dir, path.join(tf_inject[DI.resources_dir], 'ui-descriptions'))
        tf_inject.provide(DI.server_url_http, server_url_http)
        tf_inject.provide(DI.server_url_https, server_url_https)
        tf_inject.provide(DI.Server, Server(tf_inject[DI.server_url_https]))
        tf_inject.provide(DI.webkit_cookies_file_path, path.join(tf_inject[DI.app_tmp_dir], '.cookies'))

        logging.basicConfig(level=logging.WARNING)
        
    def start_app(self):
        self.client_app_settings, self.user_had_to_login = RealClientAppSettings.get(tf_inject[DI.app_home])
        tf_inject.provide(DI.ClientAppSettings, self.client_app_settings)

        try:
            self._load_webkit_cookies()
            
            TopfeedGtkIconFactory().initialize()
            
            self._server_session = ServerSession(self.client_app_settings.session_id)
            tf_inject.provide(DI.ServerSession, self._server_session)
            self._user_settings = self._server_session.get_user_settings()
            tf_inject.provide(DI.ServerUserSettings, self._user_settings)
            
            self._feeds_fetcher = tf_inject[DI.FeedsFetcher]    # TODO: Move this to where it is used.
            
            self._current_channels = self._user_settings.channels   # TODO: Remove from here.

            normal_feeds_category = self._create_normal_feed_items_category()
            faved_feeds_category  = self._create_faved_feed_items_category()
            self._feeds_window = FeedItemsWindow(normal_feeds_category, faved_feeds_category)

            self._feeds_window.stick()
            #self._feeds_window.set_keep_below(True)
            self._feeds_window.set_skip_pager_hint(True)
            self._feeds_window.set_skip_taskbar_hint(True)
            self._feeds_window.set_can_focus(False)
            self._feeds_window.show_all()

            if self.user_had_to_login:
                # If we don't assign to a field, Gtk will wrongly GC the guide while it is in use.
                self.starters_guide = StartersGuide()
                self.starters_guide.show_all()

            Gtk.main()
            self.on_app_quit()
            
        except AuthorizationError:
            self.client_app_settings.session_id = LoginDialoge().run()
            self.client_app_settings.save_to_disk()
            tf_inject.unbind_all()
            Controller(sys.argv[1], sys.argv[2]).start_app()

    def _create_normal_feed_items_category(self):
        def feed_score_func(feed):
            def time_penalty(hours_passed):
                return (hours_passed ** 3) / 3000

            upvote_factor = 3
            dnvote_factor = -4
            read_factor = 1
            already_read_penalty = -1000000
            pub_date = feed.pub_date if feed.pub_date else datetime.min
            hours_passed = (datetime.now() - pub_date).total_seconds() / 3600.0

            score = (((feed.channel.priority * 200) - 100) ** 2) / 150 * (1 if feed.channel.priority > 0.5 else -1)
            score -= time_penalty(hours_passed)
            score += feed.num_upvotes * upvote_factor
            score += feed.num_dnvotes * dnvote_factor
            score += feed.num_reads   * read_factor
            if feed.is_read_by_user:
                score += already_read_penalty
            return score

        def fetch_normal_feed_items():
             # TODO: Remove the repetitive code from the following few lines.
            web_feeds_thread_q = Queue()
            web_feeds_thread = Thread(target = lambda: web_feeds_thread_q.put(self._feeds_fetcher.get_all_feeds(self._current_channels)))
            web_feeds_thread.start()
            server_feeds_thread_q = Queue()
            server_feeds_thread = Thread(target = lambda: server_feeds_thread_q.put(self._server_session.get_server_feed_data()))
            server_feeds_thread.start()
            favorite_feed_infos_q = Queue()
            favorite_feed_infos_fetcher_thread = Thread(target = lambda: favorite_feed_infos_q.put(self._server_session.get_favorite_feed_infos()))
            favorite_feed_infos_fetcher_thread.start()

            feeds = web_feeds_thread_q.get()
            server_feed_data = server_feeds_thread_q.get()
            favorite_feed_infos = favorite_feed_infos_q.get()

            def add_topfeed_server_data_to_feeds(unannotated_feeds, server_feed_infos, favorite_feed_infos):
                for feed in unannotated_feeds:
                    for feed_info in server_feed_infos:
                        if feed.link == feed_info.link:
                            feed.num_upvotes = feed_info.num_upvotes
                            feed.num_dnvotes = feed_info.num_dnvotes
                            feed.num_reads   = feed_info.num_reads
                            feed.user_vote   = feed_info.user_vote
                            feed.is_read_by_user = feed_info.is_read_by_user
                            break

                    for fav_feed_info in favorite_feed_infos:
                        if feed.link == fav_feed_info.url:
                            feed.is_favorite = True

            add_topfeed_server_data_to_feeds(feeds, server_feed_data, favorite_feed_infos)
            return feeds

        return FeedItemsCategory(fetch_normal_feed_items, feed_score_func, sort_is_reveresed=True)

    def _create_faved_feed_items_category(self):
        def fetch_faved_feed_items():
            favorite_feed_infos = self._server_session.get_favorite_feed_infos()

            def existing_or_fake_channel_by(channel_url):
                for existing_chan in self._user_settings.channels:
                    if existing_chan.link == channel_url:
                        return existing_chan
                return Channel("fake_url", Color(.92, .75, .2, 1))

            return [Feed("fake_id",
                        existing_or_fake_channel_by(fav_feed_info.channel_url),
                        fav_feed_info.url,
                        title = fav_feed_info.title,
                        description = fav_feed_info.description,
                        pub_date = fav_feed_info.date_time,
                        is_favorite = True)
                    for fav_feed_info in favorite_feed_infos]

        def fav_feed_score_func(feed):
            return feed.pub_date

        category = FeedItemsCategory(fetch_faved_feed_items, fav_feed_score_func, sort_is_reveresed=True)
        category.topfeed_social_features_are_enabled = False
        return category
    
    def on_app_quit(self):
        self.client_app_settings.save_to_disk()
    
    def _load_webkit_cookies(self):
        cookiejar = Soup.CookieJarText.new(tf_inject[DI.webkit_cookies_file_path], False)
        cookiejar.set_accept_policy(Soup.CookieJarAcceptPolicy.ALWAYS)
        session = WebKit.get_default_session()
        session.add_feature(cookiejar)

    def _create_img_cache_dir_for_now_and_cleanup_olds(self, base_dir_for_cache_dir):
        ensure_directory_exists(base_dir_for_cache_dir)

        this = date.today()
        name_of_cache_dir_for_now = str(this.year) + '-' + str(this.month) + '-' + str(this.day / 7)

        # Clean-up other (older) cache_dirs.
        for entry in os.listdir(base_dir_for_cache_dir):
            entry_path = path.join(base_dir_for_cache_dir, entry)
            if os.path.isdir(entry_path) and entry != name_of_cache_dir_for_now:
                shutil.rmtree(entry_path)

        return path.join(base_dir_for_cache_dir, name_of_cache_dir_for_now)
        
if __name__ == '__main__':
    if len(sys.argv) != 3:
        raise ValueError("usage: application.py <server's http url> <server's https url>")

    Controller(sys.argv[1], sys.argv[2]).start_app()
