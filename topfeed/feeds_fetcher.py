"""
Created on Jan 19, 2013

@author: Pouria Mellati
"""
from domain_classes import Feed, FeedCreationException
from datetime import datetime
import time, logging, feedparser, os, pickle
from topfeed.utils import do_in_a_daemon_thread
from utils import remove_all_html_markup, get_img_descriptors_from_html_string
from Queue import Queue


class FeedsFetcher(object):
    """ The base of FeedsFetcher's. Used for DI. """
    pass

class RealFeedsFetcher(object):
    def __init__(self):
        pass
        
    def get_all_feeds(self, channels):
        feed_items = []

        future_fetched_channels = []
        for channel in channels:
            future_fetched_channel = Queue()
            future_fetched_channels.append(future_fetched_channel)
            def create_fetch_channel_func(channel, future_fetched_channel):
                return lambda: future_fetched_channel.put((channel, feedparser.parse(channel.link)))
            do_in_a_daemon_thread(create_fetch_channel_func(channel, future_fetched_channel))
        fetched_channels = [future_fetched_channel.get() for future_fetched_channel in future_fetched_channels]

        for channel, parsed_channel in fetched_channels:
            for entry in parsed_channel.entries:
                try:
                    feed_items.append(self._feedparser_entry_to_feed_item(entry, channel))
                except FeedCreationException as e:
                    logging.warn("Couldn't translate a feed entry to a FeedItem. The entry is ignored.\n Message: " + str(e))
        return feed_items
    
    def _feedparser_entry_to_feed_item(self, entry, channel):
        feed = Feed(entry.get('id'), channel, entry.get('link'), title = remove_all_html_markup(entry.get('title')))
        feed.description = remove_all_html_markup(entry.get('description')).strip(' \t\n\r')    # TODO: These should be done by the setter in Feed.
        if entry.get('published_parsed'):
            feed.pub_date = datetime.fromtimestamp(time.mktime(entry.get('published_parsed')))
            
        img_descriptors = get_img_descriptors_from_html_string(entry.get('description'))
        if len(img_descriptors) > 0:
            feed.image_descriptor = img_descriptors[0]
            
        return feed

class DevelopmentCachingFeedsFetcher(object):
    """ Used in fast development only. Loads the set of feeds from a previously pickled file. """
    
    pickled_feeds_file = os.path.expanduser('~/.topfeed_pickled_feeds_file')
    
    def get_all_feeds(self, channels):
        if os.path.isfile(DevelopmentCachingFeedsFetcher.pickled_feeds_file):
            logging.debug('Getting the cached version of the previously fetched feeds... (Topfeed Development)')
            with open(DevelopmentCachingFeedsFetcher.pickled_feeds_file, 'r') as pickled_feeds_file:
                return pickle.load(pickled_feeds_file)
        else:
            realFetcher = RealFeedsFetcher()
            fetched_feeds = realFetcher.get_all_feeds(channels)
            with open(DevelopmentCachingFeedsFetcher.pickled_feeds_file, 'w+') as pickled_feeds_file:
                pickle.dump(fetched_feeds, pickled_feeds_file, 2)
            return fetched_feeds