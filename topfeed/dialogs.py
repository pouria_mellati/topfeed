"""
Created on Jul 28, 2013

@author: Pouria Mellati
"""
from topfeed.dependency_injection import DI
from topfeed.gui.utils import run_in_ui_thread, Color
from topfeed.utils import do_in_a_daemon_thread, do_in_non_daemon_thread, SimpleEvent
from topfeed.domain_classes import Channel
from topfeed import auto_startup
from gi.repository import Gtk, WebKit, Gdk, GdkPixbuf
from os import path
from threading import Thread
import re, math

class XmlUiDefinitionBasedDialog(object):
    """ A mixin for dialogs that will be created using Gtk.Builder, based on xml ui definitions. """

    def __init__(self, ui_file_name):
        """ The ui_file_name parameter is the name of the file containing the ui description of the dialog to be
            created. This file should be locatable in the directory that is injectable as DI.ui_descriptions_dir.
        """
        dialog_descriptor_file = path.join(tf_inject[DI.ui_descriptions_dir], ui_file_name)
        self._builder = Gtk.Builder()
        if not self._builder.add_from_file(dialog_descriptor_file):
            raise RuntimeError("Failed to create a dialog from ui description file: " + dialog_descriptor_file)

    def get_widget(self, widget_name):
        return self._builder.get_object(widget_name)

    def __getitem__(self, object_name):
        return self.get_widget(object_name)

class AboutTopfeedDialog(XmlUiDefinitionBasedDialog):
    def __init__(self):
        super(AboutTopfeedDialog, self).__init__('AboutDialog.glade')

        self._app_name = tf_inject[DI.app_name]
        self._app_version = tf_inject[DI.app_version]

        self.dialog = self['TopfeedAboutDialog']
        self.dialog.set_program_name(self._app_name)
        self.dialog.set_version(self._app_version)
        self.dialog.set_logo(GdkPixbuf.Pixbuf.new_from_file(path.join(tf_inject[DI.resources_dir], 'TopFeedIcon256.png')))

        def on_dialog_response_received(dialog, response_id):
            if response_id == Gtk.ResponseType.CANCEL:
                dialog.destroy()
        self.dialog.connect('response', on_dialog_response_received)


class SettingsDialog(XmlUiDefinitionBasedDialog):
    def __init__(self, client_app_settings = None, user_settings = None, server_session = None):
        super(SettingsDialog, self).__init__('SettingsDialog.glade')

        self._app_settings = client_app_settings or tf_inject[DI.ClientAppSettings]
        self._user_settings = user_settings or tf_inject[DI.ServerUserSettings]
        self._server = server_session or tf_inject[DI.ServerSession]

        # TODO: Most of these events should really be defined in the AppSettings class.
        self.widget_opacity_changed_event = SimpleEvent()
        self.num_widget_lines_changed_event = SimpleEvent()
        self.channel_color_changed_event  = SimpleEvent()
        self.channel_priority_changed_event = SimpleEvent()
        self.use_text_shadows_flag_toggled_event = SimpleEvent()
        self.widget_font_set_event = SimpleEvent()
        self.auto_refresh_delay_changed_event = SimpleEvent()

        self.window = self['SettingsWindow']
        self.window.connect('delete-event', self._on_closed)

        # The General Settings Tab

        self.opacity_scale = self['OpacityScale']
        self.opacity_scale.set_value(self._app_settings.feed_widget_alpha * 100)
        self.opacity_scale.connect('value-changed', self._on_opacity_scale_value_changed)

        self.num_widget_lines_spin_btn = self['WidgetTextLinesSpinButton']
        self.num_widget_lines_spin_btn.set_value(self._app_settings.widget_text_max_num_lines)
        self.num_widget_lines_spin_btn.connect('value-changed', self._on_num_widget_lines_value_changed)

        self.use_text_shadows_check_btn = self['TextShadowsCheckButton']
        self.use_text_shadows_check_btn.set_active(self._app_settings.widget_text_shadow_enabled)
        self.use_text_shadows_check_btn.connect('toggled', self._on_use_text_shadows_check_btn_toggled)

        self.run_at_login_check_btn = self['RunAtLoginCheckButton']
        self.run_at_login_check_btn.set_active(auto_startup.is_enabled())
        self.run_at_login_check_btn.connect('toggled', self._on_run_at_login_check_btn_toggled)

        self.auto_refresh_mins_spin_btn = self['AutoRefreshMinsSpinButton']
        self.auto_refresh_mins_spin_btn.set_value(math.ceil(self._app_settings.inter_auto_refresh_delay_secs / 60.0))
        self.auto_refresh_mins_spin_btn.connect('value-changed', self._on_auto_refresh_mins_spin_btn_value_changed)

        self.widget_font_btn = self['FeedWidgetFontButton']
        self.widget_font_btn.set_font_name(self._app_settings.widget_font_desc_str)
        self.widget_font_btn.connect('font-set', self._on_new_font_set)

        # The Feed Channels Tab
        self.feed_channels_list_store = self['FeedChannelsListStore']
        self._refill_feed_channels_list_store(self._user_settings.channels)

        self.feed_channels_tree_view = self['FeedChannelsTreeView']
        self.feed_channels_tree_view_selection = self.feed_channels_tree_view.get_selection()
        self.feed_channels_tree_view_selection.connect('changed', self._on_feed_channel_selection_changed)

        self.feed_channel_settings_grid = self['FeedChannelSettingsGrid']

        self.channel_color_btn = self['ChannelColorBtn']
        self.channel_color_btn.connect('color-set', self._on_channel_color_changed)

        self.channel_priority_scale = self['ChannelPriorityScale']
        self.channel_priority_scale.connect('value-changed', self._on_channel_priority_scale_value_changed)

        self.delete_feed_channel_btn = self['DeleteFeedChannelBtn']
        self.delete_feed_channel_btn.connect('clicked', self._on_feed_channel_delete_btn_clicked)

        self.add_feed_channel_btn = self['AddFeedChannelBtn']
        self.add_feed_channel_btn.connect('clicked', self._on_add_feed_channel_btn_clicked)
    
    def _refill_feed_channels_list_store(self, channels):
        self.feed_channels_list_store.clear()
        for channel in channels:
            self.feed_channels_list_store.append([channel.link])
    
    def _on_closed(self, *_):
        self._app_settings.save_to_disk()   # TODO: This is a temporary fix for ensuring that the settings are saved, even in the face of abrupt system shutdowns.
        do_in_non_daemon_thread(lambda: self._server.save_user_settings(self._user_settings))

    def _on_opacity_scale_value_changed(self, *_):
        self._app_settings.feed_widget_alpha = self.opacity_scale.get_value() / 100.0
        self.widget_opacity_changed_event()
    
    def _on_num_widget_lines_value_changed(self, *_):
        self._app_settings.widget_text_max_num_lines = self.num_widget_lines_spin_btn.get_value()
        self.num_widget_lines_changed_event()
    
    def _on_use_text_shadows_check_btn_toggled(self, *_):
        self._app_settings.widget_text_shadow_enabled = self.use_text_shadows_check_btn.get_active()
        self.use_text_shadows_flag_toggled_event()
    
    def _on_new_font_set(self, *_):
        self._app_settings.widget_font_desc_str = self.widget_font_btn.get_font_name()
        self.widget_font_set_event()
        
    def _on_feed_channel_selection_changed(self, *_):
        selected_channel = self._get_currently_selected_channel_or_None()
        if selected_channel is None:
            self.feed_channel_settings_grid.set_sensitive(False)
        else:
            self.feed_channel_settings_grid.set_sensitive(True)
            self._populate_channel_settings_for_channel(selected_channel)
    
    def _populate_channel_settings_for_channel(self, channel):
        self.channel_color_btn.set_rgba(Gdk.RGBA(*channel.color.to_tuple()))
        self.channel_priority_scale.set_value(channel.priority)
    
    def _on_channel_color_changed(self, *_):
        new_color_GdkRgba = self.channel_color_btn.get_rgba()
        channel = self._get_currently_selected_channel_or_None()
        channel.color.r = new_color_GdkRgba.red
        channel.color.g = new_color_GdkRgba.green
        channel.color.b = new_color_GdkRgba.blue
        self.channel_color_changed_event()
        
    def _on_channel_priority_scale_value_changed(self, *_):
        new_priority = self.channel_priority_scale.get_value()
        channel = self._get_currently_selected_channel_or_None()
        channel.priority = new_priority
        self.channel_priority_changed_event()
    
    def _get_currently_selected_channel_or_None(self):
        _ignored, selection_iter = self.feed_channels_tree_view_selection.get_selected()
        if selection_iter is None:
            return None
        else:
            selected_channel_link = self.feed_channels_list_store[selection_iter][0]
            for channel in self._user_settings.channels:
                if channel.link == selected_channel_link:
                    return channel
    
    def _on_feed_channel_delete_btn_clicked(self, *_):
        self._user_settings.channels.remove(self._get_currently_selected_channel_or_None())
        self._refill_feed_channels_list_store(self._user_settings.channels)
    
    def _on_add_feed_channel_btn_clicked(self, *_):
        self.window.hide()
        self.add_chan_dialog = AddFeedChannelDialog()
        self.add_chan_dialog.window.show_all()
        self.add_chan_dialog.window.connect('destroy', self._on_add_channel_dialog_ended)
    
    def _on_add_channel_dialog_ended(self, *args):
        added_channel = self.add_chan_dialog.added_channel
        if added_channel:
            self._user_settings.channels.add(added_channel)
            self._refill_feed_channels_list_store(self._user_settings.channels)
        self.window.show_all()

    def _on_run_at_login_check_btn_toggled(self, *_):
        if self.run_at_login_check_btn.get_active():
            auto_startup.enable()
        else:
            auto_startup.disable()

    def _on_auto_refresh_mins_spin_btn_value_changed(self, *_):
        self._app_settings.inter_auto_refresh_delay_secs = self.auto_refresh_mins_spin_btn.get_value() * 60
        self.auto_refresh_delay_changed_event()


class AddFeedChannelDialog(XmlUiDefinitionBasedDialog):
    """ Also checks the OK-ness of the feed channel url that the user enters.
      
        A client should manually show_all the window of this dialog, and connect to the window's destroy
        event. On destruction, if the user has successfully added a channel, the added_channel field will hold
        that channel object, else added_channel is None. """
        
    def __init__(self):
        super(AddFeedChannelDialog, self).__init__('AddFeedChannelDialog.glade')

        self.added_channel = None
        
        self.window = self['Window']
        self.main_box = self['MainBox']
        
        self.feed_channel_url_entry = self['FeedChannelUrlEntry']
        
        self.add_feed_channel_btn = self['AddFeedButton']
        self.add_feed_channel_btn.connect('clicked', self._on_add_feed_channel_btn_clicked)
        
        self.channel_color_btn = self['ChannelColorBtn']
        self.channel_priority_scale = self['ChannelPriorityScale']
    
    def _on_add_feed_channel_btn_clicked(self, *_):
        """ TODO: Use a utility class to simplify event handlers like this around the code, where two ui update
            sequences sandwich a long-running operation that runs outside the ui thread. """ 
         
        import feedparser
        
        feed_url_entered = self.feed_channel_url_entry.get_text().strip()
        if not feed_url_entered:
            return
        
        if not (feed_url_entered.startswith('http://') or feed_url_entered.startswith('https://')):
            feed_url_entered = 'http://' + feed_url_entered 
        
        self.main_box.set_sensitive(False)
        original_btn_label = self.add_feed_channel_btn.get_label()
        self.add_feed_channel_btn.set_label('Verifying ...')
        
        def process_add_feed_request_thread():
            parse_result = feedparser.parse(feed_url_entered)
            
            def update_ui(*_):
                if not parse_result.bozo:   # Not bozo: Ok.
                    self.added_channel = self._create_channel_from_this_url_and_ui_entries(feed_url_entered)
                    self.window.destroy()
                    
                elif len(parse_result.entries) > 0:     # Bozo, but entries were found.
                    d = Gtk.MessageDialog(None, Gtk.DialogFlags.MODAL, Gtk.MessageType.WARNING, Gtk.ButtonsType.YES_NO,
                                          "The feed found in the entered address seems to be badly formed. It is not recommended to add it.\nWould you like to add it anyway?")
                    user_choice = d.run()
                    d.destroy()
                    
                    if user_choice == Gtk.ResponseType.YES:
                        self.added_channel = self._create_channel_from_this_url_and_ui_entries(feed_url_entered)
                        self.window.destroy()                
                
                else:   # Bozo, and no entries: Probably not a proper feed url.
                    d = Gtk.MessageDialog(None, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK,
                                          "Couldn't find a feed at the specified address.")
                    d.run()
                    d.destroy()
                
                self.add_feed_channel_btn.set_label(original_btn_label)
                self.main_box.set_sensitive(True)
            
            run_in_ui_thread(update_ui)
        do_in_a_daemon_thread(process_add_feed_request_thread)
    
    def _create_channel_from_this_url_and_ui_entries(self, channel_url):
        c_gdk = self.channel_color_btn.get_rgba()
        channel_color = Color(c_gdk.red, c_gdk.green, c_gdk.blue, c_gdk.alpha)
        return Channel(channel_url, channel_color, self.channel_priority_scale.get_value())
        
class LoginDialoge(object):
    """ Invoking run() Shows a wizard that will get the user to either sign in or register. The
        result of the invocation is a session id. If the user closes the dialoge without logging
        in, a LoginAbruptlyEndedError is raised.
    """
    
    def __init__(self):
        self._session_id = None
    
    class LoginAbruptlyEndedError(RuntimeError): pass
        
    def run(self):
        self._window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        self._window.set_position(Gtk.WindowPosition.CENTER)
        self._window.set_title("Welcome to {0}!".format(tf_inject[DI.app_name]))
        self._window.connect("delete-event", self._on_wizard_abruptly_ended)
        self.grid = Gtk.Grid(margin=10)
        self._window.add(self.grid)
        wizard_label = Gtk.Label(xalign=0, margin_bottom=10, wrap=True)  # halign didn't work.
        wizard_markup = "Enter your user and pass below to get started."
        wizard_label.set_markup(wizard_markup)
        self.grid.attach(wizard_label, 0, 0, 2, 1)
        self.username_entry = Gtk.Entry(hexpand=True)
        self.password_entry = Gtk.Entry()
        self.password_entry.set_visibility(False)
        self.grid.attach(self.username_entry, 1, 1, 1, 1)
        self.grid.attach_next_to(self.password_entry, self.username_entry, Gtk.PositionType.BOTTOM, 1, 1)
        self.grid.attach_next_to(Gtk.Label("Username", halign=Gtk.Align.START, margin_right=10), self.username_entry, Gtk.PositionType.LEFT, 1, 1)
        password_label = Gtk.Label("Password", halign=Gtk.Align.START)
        self.grid.attach_next_to(password_label, self.password_entry, Gtk.PositionType.LEFT, 1, 1)
        self.login_btn = Gtk.Button(label='Login', margin=10)
        self.login_btn.connect("button-press-event", self._on_login_btn_pressed)
        self.grid.attach_next_to(self.login_btn, password_label, Gtk.PositionType.BOTTOM, 2,1)
        
        alternative_actions_label = Gtk.Label("If you haven't already registered:", halign=Gtk.Align.START, margin_top=15)
        self.grid.attach_next_to(alternative_actions_label, self.login_btn, Gtk.PositionType.BOTTOM, 2, 1)
        
        self.register_btn = Gtk.Button(label='Register', margin_right=10, margin_left=10, margin_top=10)
        self.grid.attach_next_to(self.register_btn, alternative_actions_label, Gtk.PositionType.BOTTOM, 2, 1)
        self.register_btn.connect('button-press-event', self._on_register_btn_pressed)
        
        self._window.show_all()
        Gtk.main()
        
        if not self._session_id:
            raise self.LoginAbruptlyEndedError
        return self._session_id
    
#    def _create_client_settings_from_session_id(self, user_settings):
#        return RealClientAppSettings(self._path_to_settings_file, user_settings)
    
    def _on_login_btn_pressed(self, *_):
        self.login_btn.set_sensitive(False)
        self.login_btn.set_label("Logging In ...")
        self.grid.show_all()
        username = self.username_entry.get_text()
        password = self.password_entry.get_text()
        
        def login_and_get_settings_thread():
            from topfeed.server import AuthorizationError
            
            server = tf_inject[DI.Server]
            try:
                self._session_id = server.login(username, password)
                def on_successfully_got_settings_from_server(*_):
                    self._window.destroy()
                    Gtk.main_quit()
                
                run_in_ui_thread(on_successfully_got_settings_from_server)
            except AuthorizationError:
                run_in_ui_thread(self.create_update_ui_func_for_login_fail_with_msg("The user-name and password combination was invalid. Retry?"))
            except:
                run_in_ui_thread(self.create_update_ui_func_for_login_fail_with_msg("Couldn't reach the server! Retry?"))
        
        Thread(target=login_and_get_settings_thread).start()
    
    def _on_wizard_abruptly_ended(self, *_):
        Gtk.main_quit()
        raise self.LoginAbruptlyEndedError()
    
    def _on_register_btn_pressed(self, *_):
        self._window.hide()
        registration_window = UserRegistrationWindow()
        def on_registration_window_closed(*_):
            self._window.show_all()
        registration_window.connect('delete-event', on_registration_window_closed)
        registration_window.show_all()
    
    def create_update_ui_func_for_login_fail_with_msg(self, msg):
        def update_ui_on_login_failed(*_):
            self.login_btn.set_label(msg)
            self.login_btn.set_sensitive(True)
        return update_ui_on_login_failed

class UserRegistrationWindow(Gtk.Window):
    def __init__(self):
        super(UserRegistrationWindow, self).__init__(Gtk.WindowType.TOPLEVEL, title='Register')
        self.set_modal(True)
        self.set_default_size(330,500)
        self.set_position(Gtk.WindowPosition.CENTER)
        
        web_view_scroller = Gtk.ScrolledWindow()
        self.add(web_view_scroller)
        web_view = WebKit.WebView()
        web_view_scroller.add(web_view)
        web_view.load_uri(tf_inject[DI.server_url_https] + "/register")


class StartersGuide(XmlUiDefinitionBasedDialog):
    def __init__(self):
        super(StartersGuide, self).__init__('StartersGuide.glade')

        self._assistant = self['StartersGuideAssistant']

        self._assistant.connect('delete-event', self._destroy_me)
        self._assistant.connect('cancel', self._destroy_me)
        self._assistant.connect('close', self._destroy_me)

    def show_all(self):
        self._assistant.show_all()

    def _destroy_me(self, *_):
        self._assistant.destroy()
