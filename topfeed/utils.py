'''
Created on Feb 15, 2013

@author: Pouria Mellati
'''

from threading import Thread
from HTMLParser import HTMLParser
from gi.repository import GdkPixbuf
from os import path
from datetime import datetime
import math, time, os

class ThreadedValueAnimator(object):
    ''' Gradually and in a step-wise fashion, changes current_val to finally reach target_val.
        On each change of the current_val, the new value is passed to user supplied function
        on_new_val for processing. The value changing itself as well as the executions of on_new_val
        happen in a thread separate from the caller's.
        
        Each bound, if non-None, clamps the value of target_val when it is set.
        on_target_reached is a user-provided function that, if non-None, is called when current_val
        reaches target_val. 
    '''
    def __init__(self, target_val, current_val, step_val_change = 0.05, inter_step_delay = 0.01, **kwargs):
        self._target_val = target_val
        self._current_val = current_val
        self.on_new_val = kwargs['on_new_val']
        self.step_val_change = step_val_change
        self.inter_step_delay = inter_step_delay
        self.target_upper_bound = None
        self.target_lower_bound = None
        self.on_target_reached = None
        
        self._thread = None
        self._start_thread_if_not_alive()
    
    @property
    def target_val(self):
        return self._target_val
    @target_val.setter
    def target_val(self, new_val):
        if not self.target_lower_bound is None and new_val > self.target_upper_bound:
            new_val = self.target_upper_bound
        elif not self.target_lower_bound is None and new_val < self.target_lower_bound:
            new_val = self.target_lower_bound
            
        self._target_val = new_val
        self._start_thread_if_not_alive()
    
    @property
    def current_val(self):
        return self._current_val
    @current_val.setter
    def current_val(self, new_val):
        self._current_val = new_val
        self._start_thread_if_not_alive()
    
    def _start_thread_if_not_alive(self):
        if((not self._thread) or (not self._thread.is_alive())):
            self._thread = Thread(target=self._gradually_change_val_to_target_and_end_if_target_reached)
            self._thread.setDaemon(True)
            self._thread.start()
            
    def _gradually_change_val_to_target_and_end_if_target_reached(self):
        def dist_to_target_val():
            return self._target_val - self._current_val 
        while(dist_to_target_val()):
            if(abs(dist_to_target_val()) > self.step_val_change):
                self._current_val += math.copysign(self.step_val_change, dist_to_target_val())
                time.sleep(self.inter_step_delay)
            else:
                self._current_val = self._target_val
                if self.on_target_reached:
                    self.on_target_reached(self._current_val)
            self.on_new_val(self._current_val)


def remove_all_html_markup(string):
    class MarkupRemover(HTMLParser):
        def __init__(self):
            self.reset()
            self.string_parts = []
        def handle_data(self, d):
            self.string_parts.append(d)
        def get_clean_string(self):
            return ''.join(self.string_parts)
    remover = MarkupRemover()
    remover.feed(string)
    return remover.get_clean_string()

class ImageDescriptor(object):
    class ImageType(object):
        DISK = 1
        WEB  = 2
    def __init__(self, url, img_type, caption = None):
        if(not url or not type):
            raise ValueError('Cannot create a ImageDescriptor without a url, or a type.')
        self.url = url
        self.type = img_type
        self.caption = caption

def get_img_descriptors_from_html_string(string):
    class ImageDescriptorExtractorFromHtml(HTMLParser):
        def __init__(self):
            self.reset()
            self._web_img_descriptors = []
        def handle_starttag(self, tag, attrs):
            if(tag == 'img'):
                url = desc = None
                for (key,val) in attrs:
                    if(key == 'src'):
                        url = val
                    elif(key == 'alt'):
                        desc = val
                try:
                    self._web_img_descriptors.append(ImageDescriptor(url, ImageDescriptor.ImageType.WEB, desc))
                except:
                    pass
        def get_extracted_descriptors(self):
            return self._web_img_descriptors
    img_extractor = ImageDescriptorExtractorFromHtml()
    img_extractor.feed(string)
    return img_extractor.get_extracted_descriptors()

def do_in_a_daemon_thread(func, args_tuple=()):
    t = Thread(target=func, args = args_tuple)
    t.setDaemon(True)
    t.start()

def do_in_non_daemon_thread(func, args_tuple=()):
    t = Thread(target=func, args = args_tuple)
    t.setDaemon(False)
    t.start()

def print_run_duration(func):
    def wrapper(*args):
        before = datetime.now()
        func(*args)
        print func.__name__, '- run time:', datetime.now() - before
    return wrapper

def ensure_directory_exists(directory):
    if not path.isdir(directory):
        os.makedirs(directory)

class KeyedEqualityNHashMixin(object):
    def __eq__(self, other):
        return self.__key__() == other.__key__()
    def __ne__(self, other):
        return self.__key__() != other.__key__()
    def __hash__(self):
        return hash(self.__key__())

class SimpleEvent(object):
    ''' Works with handlers that take no parameters. '''
    def __init__(self):
        self.handlers = []
    
    def add_handler(self, handler):
        self.handlers.append(handler)
        return self
    
    def remove_handler(self, handler):
        self.handlers.remove(handler)
        return self
    
    def fire(self):
        for handler in self.handlers:
            handler()
    
    __iadd__ = add_handler
    __isub__ = remove_handler
    __call__ = fire

def iso_8601_to_datetime(joda_str):
    """ Iso 8601 is compatible with joda time DateTime.toString().
        Ignores milliseconds. An example of a time-stamp: '2013-10-16T11:35:49.057+03:30'
    """
    return datetime.strptime(joda_str.split('.')[0], "%Y-%m-%dT%H:%M:%S")

def datetime_to_iso_8601(date_time):
    return date_time.isoformat()

class CancellableFutureJob(object):
    """ Executes a function after a delay. Has a race condition that might rarely cause it to run the job even if
        it is cancelled. Uses a daemon thread: closing the app cancels the job.
    """
    def __init__(self, func, delay_secs_until_job):
        self.func = func
        self.delay_secs_until_job = delay_secs_until_job
        self._is_canceled = False

    def start(self):
        def wait_and_act_thread():
            time.sleep(self.delay_secs_until_job)
            if not self._is_canceled:
                self.func()
        do_in_a_daemon_thread(wait_and_act_thread)

    def cancel(self):
        self._is_canceled = True