"""
Created on Jan 19, 2013

@author: Pouria Mellati
"""
from topfeed.dependency_injection import DI
from topfeed.json_converters import *
from topfeed.domain_classes import FavoriteFeedInfo
from datetime import datetime
from requests.exceptions import Timeout, ConnectionError, SSLError
import requests, logging, urllib, json, atexit, time, sys
from topfeed.utils import CancellableFutureJob


class AuthorizationError(RuntimeError): pass
class CouldNotReachServerError(RuntimeError): pass

def _raise_if_response_is_bad(requests_http_response):
    if requests_http_response.status_code == 401:
        raise AuthorizationError
    requests_http_response.raise_for_status()

class Server(object):
    def __init__(self, server_url_https = None):
        self._server_url_https = server_url_https or tf_inject[DI.server_url_https]
    
    # Returns a very persistent session id.
    def login(self, username, password):
        should_verify_https = not self._server_url_https.startswith('https://localhost')
        response = requests.post(self._server_url_https + "/api/login", headers = {"username" : username, "password" : password}, verify=should_verify_https)
        _raise_if_response_is_bad(response)
        return response.text

class ServerSession(object):
    def __init__(self, session_id, server_url = None):
        if not session_id:
            raise ValueError('The session_id parameter is required.')
        self._server_url_http = server_url or tf_inject[DI.server_url_http]
        self._session_id = session_id

        self._feed_reads_to_batch_send_to_server = {}   # A map from channel urls to sets of feed urls.
        self._scheduled_batch_send_of_reads = None
        self._reschedule_next_auto_batch_send_of_reads()

        atexit.register(self._send_any_bulked_feed_reads_to_server)

    def _request(self, method_str, path_n_query, data = None, additional_headers = None, **kwargs):
        if not additional_headers:
            additional_headers = dict()

        num_tries = 0
        max_num_tries = 5

        while True:
            try:
                num_tries += 1
                response = requests.request(method_str, url = self._server_url_http + '/api/' + path_n_query,
                                        data = data,
                                        headers = dict(additional_headers, **{'session-id': self._session_id}),
                                        timeout=40,
                                        **kwargs)
            except (Timeout, SSLError, ConnectionError) as e:    # requests raises SSLError if handshake times-out.
                if num_tries >= max_num_tries:
                    raise CouldNotReachServerError(), None, sys.exc_info()[2]
                elif isinstance(e, ConnectionError):
                    retry_delay_secs = 30
                    logging.warn("Couldn't connect to TopFeed's server. Retrying in %i seconds."%retry_delay_secs)
                    time.sleep(retry_delay_secs)
                else:
                    logging.warn("Request to TopFeed's server timed-out (or SSLError). Will retry now.")
            else:
                _raise_if_response_is_bad(response)
                return response

    def _encode_url(self, url):
        return urllib.quote(url, "")

    def get_server_feed_data(self):
        self._send_any_bulked_feed_reads_to_server()
        return json_str_2_ServerFeedInfos(self._request('get', "feedinfos").text)

    def upvote(self, channel_url, feed_url):
        return self.vote(channel_url, feed_url, 1)

    def unvote(self, channel_url, feed_url):
        return self.vote(channel_url, feed_url, 0)

    def dnvote(self, channel_url, feed_url):
        return self.vote(channel_url, feed_url, -1)

    def vote(self, channel_url, feed_url, vote_code):
        """ vote_code: {-1, 0, 1} 
            Returns True if successful, and False otherwise. """
        vote_str = ''
        if vote_code == 1:
            vote_str = 'upvote'
        elif vote_code == 0:
            vote_str = 'unvote'
        elif vote_code == -1:
            vote_str = 'dnvote'
        if not vote_str:
            raise ValueError("vote_code must be one of -1, 0, 1.")

        try:
            self._request('put', "{0}?channel={1}&feed={2}".format(vote_str, self._encode_url(channel_url), self._encode_url(feed_url)))
            return True
        except:
            logging.warn("Voting failed for feed with url: " + feed_url)
            return False

    def get_user_settings(self):
        return json_str_2_UserSettings(self._request('get', 'user/settings').text)

    def save_user_settings(self, user_settings):
        self._request('put', 'user/settings', data = UserSettings_2_json_str(user_settings), additional_headers = {'content-type': 'application/json'})

    def favorite_feed(self, feed):
        fav_feed_info = FavoriteFeedInfo(
            url=feed.link,
            channel_url=feed.channel.link,
            title=feed.title,
            description=feed.description,
            date_time=datetime.now())
        self._request('put', "favorite", data=FavoriteFeedInfo_2_js_str(fav_feed_info), additional_headers={'content-type': 'application/json'})

    def unfavorite_feed(self, feed_url):
        self._request('put', "unfavorite?feed={0}".format(self._encode_url(feed_url)))

    def get_favorite_feed_infos(self):
        fav_feed_info_jsons = json.loads(self._request('get', "user/favoritefeeds").text)
        return [json_2_FavoriteFeedInfo(fav_feed_info_js) for fav_feed_info_js in fav_feed_info_jsons]

    def set_feed_as_read(self, channel_url, feed_url):
        if channel_url not in self._feed_reads_to_batch_send_to_server:
            self._feed_reads_to_batch_send_to_server[channel_url] = set()

        self._feed_reads_to_batch_send_to_server[channel_url].add(feed_url)

    def _send_any_bulked_feed_reads_to_server(self):
        if self._feed_reads_to_batch_send_to_server:
            self._request('put', 'feedreads', data = bulked_reads_2_json_str(self._feed_reads_to_batch_send_to_server),
                          additional_headers = {'content-type': 'application/json'})
            self._feed_reads_to_batch_send_to_server = {}
        self._reschedule_next_auto_batch_send_of_reads()

    def _reschedule_next_auto_batch_send_of_reads(self):
        if self._scheduled_batch_send_of_reads:
            self._scheduled_batch_send_of_reads.cancel()
        self._scheduled_batch_send_of_reads = CancellableFutureJob(lambda: self._send_any_bulked_feed_reads_to_server(), 5 * 60)
        self._scheduled_batch_send_of_reads.start()