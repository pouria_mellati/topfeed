"""
Created on Jul 28, 2013

@author: Pouria Mellati

Configures auto start-up of the app on user login. Only linux desktop environments are currently supported.
Based on http://askubuntu.com/questions/357399/letting-the-users-of-my-application-select-whether-the-app-should-start-on-login
"""
from dependency_injection import DI
from os import path, environ
import shutil, os

def is_enabled():
    return path.isfile(_get_autostart_desktop_file_path())

def enable():
    if path.isdir(_get_autostart_desktop_file_path()):
        shutil.rmtree(_get_autostart_desktop_file_path())

    with open(_get_autostart_desktop_file_path(), 'w') as f:
        f.write(
"""
[Desktop Entry]
Type=Application
Exec={0}
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name={1}
""".format(tf_inject[DI.path_to_start_script], tf_inject[DI.app_name]).strip()
        )

def disable():
    os.remove(_get_autostart_desktop_file_path())

def _get_autostart_desktop_file_path():
    return path.join(environ.get('XDG_CONFIG_HOME', path.join(path.expanduser('~'), '.config')),
                     'autostart', tf_inject[DI.app_name] + '.desktop')