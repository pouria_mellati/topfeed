"""
Created on May 24, 2013

@author: Pouria Mellati

Contains the classes related to the rectangular theme.
"""

import cairo
from gi.repository import Gtk, Gdk, GdkPixbuf, Pango, PangoCairo, GObject
from topfeed.gui.utils import pixels_to_pango_units, draw_method, create_pixbuf_from_image_descriptor
from topfeed.utils import do_in_a_daemon_thread, ensure_directory_exists
from topfeed.gui.base_widgets import AbstractFeedWidget, ItemsSorterScroller
from topfeed.dependency_injection import DI

class RectangularFeedWidget(Gtk.EventBox, AbstractFeedWidget, ItemsSorterScroller.Item):
    """ A feed widget in the form of a semi-transparent rectangle with gradient coloring.
        Don't forget to call queue_draw() after changing any of the widget's visible fields.
        
        Automatically takes the width of its parent widget.
    """    
    def __init__(self, feed, client_app_settings = None):
        if not feed:
            raise ValueError("The feed parameter cannot be null.")
        super(RectangularFeedWidget, self).__init__()
        self.feed = feed
        self.width = 100
        self.text_padding_vert = 4
        self.text_padding_horiz = 6
        self.pango_wrap_mode = Pango.WrapMode.CHAR
        self.pango_ellipsize_mode = Pango.EllipsizeMode.END
        self._client_app_settings = client_app_settings or tf_inject[DI.ClientAppSettings]
        
        self._image_pixbuf = None
        
        if self.feed.image_descriptor:
            ensure_directory_exists(tf_inject[DI.app_image_cache_dir])

            def try_set_my_pixbuf():
                downloaded_pixbuf = create_pixbuf_from_image_descriptor(self.feed.image_descriptor,
                                                                        tf_inject[DI.app_image_cache_dir])
                if downloaded_pixbuf.get_width() >= 5 and downloaded_pixbuf.get_height() >= 5:
                    self.image_pixbuf = downloaded_pixbuf

            do_in_a_daemon_thread(try_set_my_pixbuf)


        self._text_markup_color_abstract = self._create_color_abstract_widget_markup()

        self.set_app_paintable(True)
        self.connect("draw", self._draw)
        self.connect("parent-set", self._on_parent_set)

    def _on_parent_set(self, *_):
        if self.get_parent():
            self.width = self.get_parent().get_allocated_width()

    @property
    def _num_max_text_lines(self):
        return self._client_app_settings.widget_text_max_num_lines

    @property
    def image_pixbuf(self):
        return self._image_pixbuf
    
    @image_pixbuf.setter
    def image_pixbuf(self, new_pixbuf):
        self._image_pixbuf = new_pixbuf
        self.queue_draw()
    
    @draw_method
    def _draw(self, _, cr):
        def draw_background():
            cr.save()
            cr.set_operator(cairo.OPERATOR_SOURCE)
            cr.scale(width, height)
            
            alpha = self._client_app_settings.feed_widget_alpha
            gradient = cairo.LinearGradient(0,0,1,0)
            r,g,b,_ignored = self.feed.channel.color.to_tuple()
            gradient.add_color_stop_rgba(0, r, g, b, alpha)
            gradient.add_color_stop_rgba(1, 0.1, 0.1, 0.1, alpha)
            cr.rectangle(0, 0, 1, 1)
            cr.set_source(gradient)
            cr.fill()
            cr.restore()
        
        def draw_texts():
            text_color_str = 'grey' if self.feed.is_read_by_user else 'white'
            
            cr.save()
            cr.move_to(self.text_padding_horiz, self.text_padding_vert)
            if self.image_pixbuf:
                cr.rel_move_to(height, 0)  # Move horizontally to leave room for the _ideal_height*_ideal_height sized image.
            
            cr.rel_move_to(1,1)
            
            layout = self._new_layout(cr, self._text_markup_color_abstract.format(color='black'))
            layout.set_width(pixels_to_pango_units(width - cr.get_current_point()[0] - self.text_padding_horiz))
            layout.set_height(pixels_to_pango_units(height - cr.get_current_point()[1]))
            layout.set_wrap(self.pango_wrap_mode)
            layout.set_ellipsize(self.pango_ellipsize_mode)
            PangoCairo.update_layout(cr, layout)
            
            if self._client_app_settings.widget_text_shadow_enabled:
                PangoCairo.show_layout(cr, layout)
                
            cr.rel_move_to(-1,-1)
            layout.set_markup(self._text_markup_color_abstract.format(color=text_color_str))
            PangoCairo.show_layout(cr, layout)

            cr.restore()
        
        def draw_image():
            cr.save()
            target_img_dim = height_to_request if height <= 1 else height
            pixbuf = self.image_pixbuf
            smaller_dim = min(pixbuf.get_height(), pixbuf.get_width())
            scale_factor = float(target_img_dim) / float(smaller_dim)
            scaled_buf = pixbuf.scale_simple(pixbuf.get_width() * scale_factor, pixbuf.get_height() * scale_factor, GdkPixbuf.InterpType.BILINEAR)
            width_is_smaller = True if scaled_buf.get_width() < scaled_buf.get_height() else False
            extra_length = max(scaled_buf.get_width(), scaled_buf.get_height()) - target_img_dim
            if width_is_smaller: 
                Gdk.cairo_set_source_pixbuf(cr, scaled_buf, 0, extra_length / -2)
            else:
                Gdk.cairo_set_source_pixbuf(cr, scaled_buf, extra_length / -2, 0)
            cr.rectangle(0,0,target_img_dim,target_img_dim)
            cr.fill()
            cr.restore()
        
        height_to_request = self.get_ideal_height()
        self.set_size_request(self.width, height_to_request)
        
        width, height = (self.get_allocated_width(), self.get_allocated_height())
            
        draw_background()
        draw_texts()
        if self.image_pixbuf:
            draw_image()
        
    def _new_layout(self, cr, markup = ""):
        layout = PangoCairo.create_layout(cr)
        layout.set_font_description(Pango.font_description_from_string (self._client_app_settings.widget_font_desc_str))
        layout.set_markup(markup)
        return layout
    
    def _get_line_height_pixels(self):
        return self._get_text_height_pixels_assuming_no_image("<span><b>S</b></span>")
    
    def _get_text_height_pixels_assuming_no_image(self, text):
        temp_cr = cairo.Context(cairo.ImageSurface(cairo.FORMAT_ARGB32, 10, 10))
        sample_layout = self._new_layout(temp_cr, text)
        sample_layout.set_width(pixels_to_pango_units(self.width - self.text_padding_horiz))
        sample_layout.set_height(100000) 
        sample_layout.set_wrap(self.pango_wrap_mode)
        sample_layout.set_ellipsize(self.pango_ellipsize_mode)
        PangoCairo.update_layout (temp_cr, sample_layout)
        width_pixels, height_pixels = sample_layout.get_pixel_size()
        return 0 if width_pixels == 0 else height_pixels
    
    def _get_max_height_allowed(self):
        return self._get_line_height_pixels() * self._num_max_text_lines + self.text_padding_vert * 2
    
    def _get_ideal_height_assuming_no_image(self):
        txt_height = self._get_text_height_pixels_assuming_no_image(self._text_markup_color_abstract.format(color='white'))
        return txt_height + self.text_padding_vert * 2
    
    def get_ideal_height(self):
        if self.feed.image_descriptor:
            return self._get_max_height_allowed()
        else:
            return min(self._get_ideal_height_assuming_no_image(), self._get_max_height_allowed())
    
    def on_container_resized(self, width, height):
        self.width = width

    def _create_color_abstract_widget_markup(self):
        def prep_text(text):
            return GObject.markup_escape_text(" ".join(text.split()))

        return "<span stretch='condensed' weight='bold' color='{color}'>" + prep_text(self.feed.title) + "</span>" + \
                "<span weight='normal' color='{color}'> - " + prep_text(self.feed.description) + "</span>"