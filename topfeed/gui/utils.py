"""
Created on Feb 16, 2013

@author: Pouria Mellati
"""
# TODO: Rename to gtkUtils, and only let GtkSpecific stuff remain here. The rest should go to topfeed.utils.

from gi.repository import Pango, Gdk, GLib, GdkPixbuf
from topfeed.utils import KeyedEqualityNHashMixin, ImageDescriptor
from os import path
from datetime import datetime
import cairo, requests, os


def pixels_to_pango_units(pixels):
    return Pango.SCALE * (pixels)

def run_in_ui_thread(fn):
    Gdk.threads_add_idle(GLib.PRIORITY_DEFAULT_IDLE, fn, None)

class Color(KeyedEqualityNHashMixin):
    def __init__(self, r, g, b ,a):
        for component in [r,g,b,a]:
            if (not 0 <= component <= 1):
                raise ValueError("Color components must be between 0 and 1.")
            
        self.r = r
        self.g = g
        self.b = b
        self.a = a
    
    def __key__(self):
        return (self.r, self.g, self.b, self.a)
    
    def to_tuple(self):
        return (self.r, self.g, self.b, self.a)

class BoxDimensions(object):
    def __init__(self, left, top, width, height):
        self.left = left
        self.top = top
        self.width = width
        self.height = height
    
    def __repr__(self, *args, **kwargs):
        return "BoxDimensions(left: " + str(self.left) + ', top: ' + str(self.top) + ', width: ' + str(self.width) + ', height: ' + str(self.height) + ')'
    
    @staticmethod
    def by_right_and_bottom(left, top, right, bottom):
        return BoxDimensions(left, top, right - left, bottom - top)
    
    @staticmethod
    def by_width_and_height(left, top, width, height):
        return BoxDimensions(left, top, width, height)
    
    @property
    def right(self):
        return self.left + self.width
    @property
    def bottom(self):
        return self.top + self.height
    
    def to_tuple_width_and_height(self):
        return (self.left, self.top, self.width, self.height)
    
    def to_tuple_right_and_bottom(self):
        return (self.left, self.top, self.right, self.bottom)
    
    def contains_point(self, left, top):
        return self.left <= left <= self.right and self.top <= top <= self.bottom

def get_text_size(text, font_size):
    ''' Convenient function for getting the basic size of a piece of text. '''
    cr = cairo.Context(cairo.ImageSurface(cairo.FORMAT_ARGB32, 10, 10))
    cr.set_font_size(font_size)
    ign1, ign2, width, height, ign3, ign4 = cr.text_extents(text)
    return (width, height)

def draw_method(draw_fn):
    def wrapper(self, widget, cr):
        cr.save()
        original_ret_val = draw_fn(self, widget, cr)
        cr.restore()
        return original_ret_val
    return wrapper

# TODO: Remove the nonsense of image descriptors. We only need Web Image support anyway.
def create_pixbuf_from_image_descriptor(img_desc, temp_dir_for_images, proxies = {}):
    if not img_desc.type == ImageDescriptor.ImageType.WEB:
        raise NotImplementedError('Currently, only web images are supported.')

    def valid_file_name_from_url(url):
        import base64
        return base64.urlsafe_b64encode(url)

    canonical_file_path = path.join(temp_dir_for_images, valid_file_name_from_url(img_desc.url))

    if not path.isfile(canonical_file_path):
        tmp_img_path = path.join(temp_dir_for_images, 'tmp_image' + str(datetime.now()) )
        tmp_img_file = open(tmp_img_path, 'wb')
        tmp_img_file.write(requests.get(img_desc.url, proxies = proxies).content)
        tmp_img_file.close()
        if not path.isfile(canonical_file_path):
            os.rename(tmp_img_path, canonical_file_path)
        else:
            os.remove(tmp_img_path)

    return GdkPixbuf.Pixbuf.new_from_file(canonical_file_path)