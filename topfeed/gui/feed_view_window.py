"""
Created on May 20, 2013

@author: Pouria Mellati
"""

from gi.repository import Gtk, Gdk
from gi.repository import WebKit
from topfeed.dependency_injection import DI
from topfeed.utils import do_in_a_daemon_thread, SimpleEvent
from topfeed.gui.utils import run_in_ui_thread
import webbrowser

class FeedViewWindow(Gtk.Window):
    def __init__(self, feed, dimensions, app_settings = None, topfeed_session = None, topfeed_social_features_are_enabled = True):
        super(FeedViewWindow, self).__init__(Gtk.WindowType.TOPLEVEL, title=feed.title)

        # Prevents what seems like a bug. On ui updates of windows that should be destroyed, the ui of the current window
        # occasionally receives weird updates.
        self._pending_ui_updates_are_cancelled = False
        
        self._feed = feed
        self._topfeed_social_features_are_enabled = topfeed_social_features_are_enabled

        self._app_settings = app_settings or tf_inject[DI.ClientAppSettings]
        self._server = topfeed_session or tf_inject[DI.ServerSession]

        self.user_scroll_event = SimpleEvent()

        self.set_default_size(dimensions.width, dimensions.height)
        self.move(dimensions.left, dimensions.top)
        
        main_hbox_container = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 5)
        self.add(main_hbox_container)
        
        toolbar = Gtk.Toolbar(orientation = Gtk.Orientation.VERTICAL)
        toolbar.set_style(Gtk.ToolbarStyle.ICONS)
        main_hbox_container.pack_start(toolbar, False, False, 0)
        
        self.show_previous_item_btn = Gtk.ToolButton.new_from_stock(Gtk.STOCK_GO_UP)
        self.show_next_item_btn = Gtk.ToolButton.new_from_stock(Gtk.STOCK_GO_DOWN)
        toolbar.insert(self.show_previous_item_btn, -1)
        toolbar.insert(self.show_next_item_btn, -1)
        
        toolbar.insert(Gtk.SeparatorToolItem(),-1)

        if self._topfeed_social_features_are_enabled:
            self._upvote_toggle_btn = Gtk.ToggleToolButton.new_from_stock('topfeed-upvote-icon')
            toolbar.insert(self._upvote_toggle_btn, -1)    # TODO: Use enums instead of strings.
            self._upvote_toggle_btn.set_tooltip_text('Up-Vote')
            if feed.user_vote == 1:
                # This has to be called before the toggle signal is connected to, otherwise the handler is called.
                self._upvote_toggle_btn.set_active(True)
            self._upvote_toggle_btn.connect('toggled', self._on_upvote_toggled)

            votes_balance_tool_item = Gtk.ToolItem()
            self._votes_balance_tool_item_entry = Gtk.Entry(text = str(self._feed.votes_balance), editable = False, sensitive = False)
            self._votes_balance_tool_item_entry.set_width_chars(3)
            votes_balance_tool_item.add(self._votes_balance_tool_item_entry)
            toolbar.insert(votes_balance_tool_item, -1)

            self._dnvote_toggle_btn = Gtk.ToggleToolButton.new_from_stock('topfeed-dnvote-icon')
            toolbar.insert(self._dnvote_toggle_btn, -1)
            self._dnvote_toggle_btn.set_tooltip_text('Down-Vote')
            if feed.user_vote == -1:
                # This has to be called before the toggle signal is connected to, otherwise the handler is called.
                self._dnvote_toggle_btn.set_active(True)
            self._dnvote_toggle_btn.connect('toggled', self._on_dnvote_toggled)

            toolbar.insert(Gtk.SeparatorToolItem(),-1)
            
        self._make_fav_toggle_tool_btn = Gtk.ToggleToolButton.new_from_stock('topfeed-fave_inactive_icon')
        if self._feed.is_favorite:
            self._make_fav_toggle_tool_btn.set_active(True)
            self._make_fav_toggle_tool_btn.set_stock_id('topfeed-fave_active_icon')
            
        self._make_fav_toggle_tool_btn.connect('toggled', self._on_make_fav_toggle_btn_toggled)
        self._make_fav_toggle_tool_btn.set_tooltip_text('"Favorite" This For Later Reading')
        toolbar.insert(self._make_fav_toggle_tool_btn, -1)

        separator_as_spring = Gtk.SeparatorToolItem()
        separator_as_spring.set_expand(True)
        separator_as_spring.set_draw(False)
        toolbar.insert(separator_as_spring, -1)

        open_in_browser_tool_btn = Gtk.ToolButton.new_from_stock('topfeed-open_in_browser_icon')
        toolbar.insert(open_in_browser_tool_btn, -1)
        open_in_browser_tool_btn.set_tooltip_text('Open In Browser')
        open_in_browser_tool_btn.connect('clicked', lambda *_: webbrowser.open(self._get_current_url()))

        share_tool_item = Gtk.ToolItem.new()
        toolbar.insert(share_tool_item, -1)
        self.config_share_button(share_tool_item)

        self.feed_web_view_scroll_wrapper = Gtk.ScrolledWindow()
        main_hbox_container.pack_start(self.feed_web_view_scroll_wrapper, True, True, 0)
        self.feed_web_view_scroll_wrapper.get_vadjustment().connect('value-changed', lambda *_: self.user_scroll_event())
        self.feed_web_view_scroll_wrapper.get_hadjustment().connect('value-changed', lambda *_: self.user_scroll_event())
        
        self._placeholder_web_view = WebKit.WebView()
        self.feed_web_view_scroll_wrapper.add(self._placeholder_web_view)
        self._placeholder_web_view.load_html_string(self._create_place_holder_html(), "")
        
        self.feed_web_view = WebKit.WebView()
        self.feed_web_view.connect('notify::load-status', self._on_feed_web_view_load_changed)
        self.feed_web_view.load_uri(self._feed.link)

        # Gtk, doesn't provide a way to (de)activate a toolbar toggle button, without raising its handler.
        # This field tells the handlers of voting buttons to do nothing once.
        self._dont_handle_vote_btn_toggle_once = False

        self.connect('delete-event', lambda *_: self.destroy())

    def _on_make_fav_toggle_btn_toggled(self, *_):
        self._make_fav_toggle_tool_btn.set_sensitive(False)
        def toggle_faveness_thread_fun(*_):
            if self._make_fav_toggle_tool_btn.get_active():
                self._server.favorite_feed(self._feed)
                self._feed.is_favorite = True
            else:
                self._server.unfavorite_feed(self._feed.link)
                self._feed.is_favorite = False
            
            def update_ui(*_):
                if self._pending_ui_updates_are_cancelled:  # Prevents some bug that is probably in Gtk.
                    return

                self._make_fav_toggle_tool_btn.set_sensitive(True)
                if self._make_fav_toggle_tool_btn.get_active():
                    self._make_fav_toggle_tool_btn.set_stock_id('topfeed-fave_active_icon')
                else:
                    self._make_fav_toggle_tool_btn.set_stock_id('topfeed-fave_inactive_icon')

            run_in_ui_thread(update_ui)
        do_in_a_daemon_thread(toggle_faveness_thread_fun)

    def _on_upvote_toggled(self, *args):
        if self._dont_handle_vote_btn_toggle_once:
            self._dont_handle_vote_btn_toggle_once = False
            return
        self._set_voting_btns_sensitivity(False)
        if self._upvote_toggle_btn.get_active():
            do_in_a_daemon_thread(self._create_vote_thread_func(1, self._upvote_toggle_btn, self._dnvote_toggle_btn))
        else:
            do_in_a_daemon_thread(self._create_vote_thread_func(0, self._upvote_toggle_btn, self._dnvote_toggle_btn))
    
    def _on_dnvote_toggled(self, *args):
        if self._dont_handle_vote_btn_toggle_once:
            self._dont_handle_vote_btn_toggle_once = False
            return
        self._set_voting_btns_sensitivity(False)
        if self._dnvote_toggle_btn.get_active():
            do_in_a_daemon_thread(self._create_vote_thread_func(-1, self._dnvote_toggle_btn, self._upvote_toggle_btn))
        else:
            do_in_a_daemon_thread(self._create_vote_thread_func( 0, self._dnvote_toggle_btn, self._upvote_toggle_btn))
    
    def _create_vote_thread_func(self, vote_code, toggle_tool_btn_that_was_toggled, toggle_tool_btn_that_must_deactivate_on_success):
        def created_func():
            voting_was_successful = self._server.vote(self._feed.channel.link, self._feed.link, vote_code)
            if voting_was_successful:
                self._feed.set_user_vote(vote_code)

            def update_ui(*_):
                if self._pending_ui_updates_are_cancelled:  # Prevents some bug that is probably in Gtk.
                    return

                if voting_was_successful:
                    self._votes_balance_tool_item_entry.set_text(str(self._feed.votes_balance))
                    if toggle_tool_btn_that_must_deactivate_on_success.get_active():
                        self._dont_handle_vote_btn_toggle_once = True
                        toggle_tool_btn_that_must_deactivate_on_success.set_active(False)
                else:
                    self._dont_handle_vote_btn_toggle_once = True
                    toggle_tool_btn_that_was_toggled.set_active(not toggle_tool_btn_that_was_toggled.get_active())
                self._set_voting_btns_sensitivity(True)
            run_in_ui_thread(update_ui)
        return created_func
    
    def _set_voting_btns_sensitivity(self, sensitivityBool):
        self._upvote_toggle_btn.set_sensitive(sensitivityBool)
        self._dnvote_toggle_btn.set_sensitive(sensitivityBool)

    def _on_feed_web_view_load_changed(self, web_view, prop):
        load_status = web_view.get_property(prop.name)
        if load_status == WebKit.LoadStatus.FIRST_VISUALLY_NON_EMPTY_LAYOUT:
            if self.feed_web_view_scroll_wrapper.get_child():
                self.feed_web_view_scroll_wrapper.remove(self.feed_web_view_scroll_wrapper.get_child())
            self.feed_web_view_scroll_wrapper.add(self.feed_web_view)
            self.feed_web_view_scroll_wrapper.show_all()
    
    def _create_place_holder_html(self):
        return """<html>
            <head>
                <style type="text/css">
                    h2 {
                        font-family: Georgia;
                        color: #006600;
                        letter-spacing: 1.4px;
                        border-bottom: solid 1px #006600;
                        text-transform: uppercase;
                        font-weight: lighter;
                    }
                    
                    .description {
                        margin: 100px;
                        max-width: 500px;
                        font-family: Verdana, sans-serif;
                        line-height: 1.5;
                        letter-spacing: .25px;
                    }
                    
                    body {
                        background: #EEEEEE;
                    }
                </style>
            <head> """ + """
            <body>
                <div class="description">
                    <i>Loading Actual Page ...</i>
                    <h2>{0}</h2>
                    {1}
                </div>
            </body>
        </html> """.format(self._feed.title.encode('utf-8'), self._feed.description.encode('utf-8'))
    
    def _get_current_url(self):
        return self.feed_web_view.get_uri() or self._feed.link
    
    def config_share_button(self, share_tool_item):
        share_menu_btn = Gtk.MenuButton()
        share_tool_item.add(share_menu_btn)
        share_menu_btn.set_direction(Gtk.ArrowType.RIGHT)
        share_menu_btn.set_relief(Gtk.ReliefStyle.NONE)
        
        share_menu = Gtk.Menu()
        share_menu.set_valign(Gtk.Align.END)
        share_menu_btn.set_popup(share_menu)
        share_menu_btn.add(Gtk.Image.new_from_stock('topfeed-share_icon', Gtk.IconSize.BUTTON))
        
        def create_share_target_menu_item(stock_icon_name):
            menu_item = Gtk.MenuItem.new()
            menu_item.add(Gtk.Image.new_from_stock(stock_icon_name, Gtk.IconSize.DIALOG))
            menu_item.set_margin_left(10)
            menu_item.set_margin_top(10)
            menu_item.set_margin_bottom(10)
            menu_item.set_margin_right(10)
            return menu_item
        
        import urllib
        
        facebook_menu_item = create_share_target_menu_item('topfeed-facebook_icon')
        share_menu.attach(facebook_menu_item, 0,1, 0,1)
        def on_fb_share_clicked(*_):
            fb_share_link = """https://www.facebook.com/sharer/sharer.php?u=""" + urllib.quote(self._get_current_url())
            self._show_share_web_view(fb_share_link, "Share This On Facebook!")
        facebook_menu_item.connect('activate', on_fb_share_clicked)
        
        twitter_menu_item = create_share_target_menu_item('topfeed-twitter_icon')
        share_menu.attach(twitter_menu_item, 1,2, 0,1)
        def on_twitter_share_clicked(*_):
            tw_share_link = """https://twitter.com/intent/tweet?url=""" + urllib.quote(self._get_current_url())
            self._show_share_web_view(tw_share_link, "Tweet it!")
        twitter_menu_item.connect('activate', on_twitter_share_clicked)
        
        share_menu.show_all()
    
    def _show_share_web_view(self, url, window_title):
        window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        window.set_title(window_title)
        window.set_default_size(self.get_screen().get_width() / 2.5, self.get_screen().get_height() / 2.5)
        window.set_modal(True)
        web_view = WebKit.WebView()
        window.add(web_view)
        def delete_web_view_and_window(*_):
            web_view.destroy()
            window.destroy()
        web_view.connect('close-web-view', delete_web_view_and_window)
        window.connect('delete-event', delete_web_view_and_window)
        web_view.load_uri(url)
        window.show_all()

    def destroy(self):
        self._pending_ui_updates_are_cancelled = True

        self._placeholder_web_view.destroy()
        self.feed_web_view.destroy()

        super(FeedViewWindow, self).destroy()