"""
Created on May 29, 2013

@author: Pouria Mellati
"""
from gi.repository import Gtk, GdkPixbuf
from os import path
from topfeed.dependency_injection import DI


class TopfeedGtkIconFactory(Gtk.IconFactory):
    """ Defines a set of 'stock' icons, alongside the image files for those icons.
    These icons are then easily usable, for example in toolbars. """
     
    def __init__(self, icons_dir=None):
        super(TopfeedGtkIconFactory, self).__init__()
        icons_dir = icons_dir or tf_inject[DI.resources_dir]
        self.add('topfeed-upvote-icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'upvote_icon'))))
        self.add('topfeed-dnvote-icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'dnvote_icon'))))
        self.add('topfeed-fave_active_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'fave_active_icon'))))
        self.add('topfeed-fave_inactive_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'fave_inactive_icon'))))
        self.add('topfeed_comments_active_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'Comments-Active.png'))))
        self.add('topfeed_comments_inactive_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'Comments-Inactive.png'))))
        self.add('topfeed_app_menu_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'app_menu_icon.png'))))
        self.add('topfeed-open_in_browser_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'open_in_browser_icon'))))
        self.add('topfeed-share_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'share_icon'))))
        self.add('topfeed-facebook_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'facebook_icon'))))
        self.add('topfeed-twitter_icon', Gtk.IconSet.new_from_pixbuf(GdkPixbuf.Pixbuf.new_from_file(path.join(icons_dir, 'twitter_icon'))))
    
    def initialize(self):
        self.add_default()