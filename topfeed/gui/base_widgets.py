"""
Created on Jan 9, 2013

@author: Pouria Mellati
"""
import cairo, time, math, os
from gi.repository import Gtk, Gdk, WebKit
from utils import run_in_ui_thread, BoxDimensions
from topfeed.utils import ThreadedValueAnimator, do_in_a_daemon_thread, SimpleEvent, CancellableFutureJob
from topfeed.gui.feed_view_window import FeedViewWindow
from topfeed.dependency_injection import DI
from topfeed.dialogs import SettingsDialog, AboutTopfeedDialog, StartersGuide


def draw_cleared_bg(widget, cr):
    cr.save()
    cr.set_operator(cairo.OPERATOR_CLEAR)
    cr.paint()
    cr.restore()

class ItemsSorterScroller(Gtk.Layout):
    """ A vertical container that sorts subclasses of the Item inner class using a provided sort_key_fn. Also
        provides the ability to scroll. On resize, calls the settable self.item_on_container_resized. """
        
    class Item(object):
        def get_ideal_height(self): raise NotImplementedError()
         
    def __init__(self, sort_key_fn = None, sort_is_reversed = False, item_h_margin = 0, item_v_margin = 2):
        super(ItemsSorterScroller, self).__init__()
        self.sort_key_fn = sort_key_fn
        self.sort_is_reversed = sort_is_reversed
        self.item_h_margin = item_h_margin
        self.item_v_margin = item_v_margin 
        self._items = []
        self.item_on_container_resized = self._prev_width = self._prev_height = None
        self._new_item_was_added = False
        self.set_app_paintable(True)
        self.connect("draw", draw_cleared_bg)
        self.connect("size-allocate", self._on_resize)
        
        self._v_scroll_adjustment = self.get_vadjustment()
        self._v_scroll_adjustment.connect('changed', self._reconfig_v_scroll_value_animator)
        def on_new_v_scroll_value(new_val):
            run_in_ui_thread(lambda *args: self._v_scroll_adjustment.set_value(new_val))        
        self._v_scroll_value_animator = ThreadedValueAnimator(0, 0, on_new_val = on_new_v_scroll_value)
    
    def _reconfig_v_scroll_value_animator(self, *_):
        fps = 60
        duration = 0.1
        self._v_scroll_adjustment.set_step_increment(self.get_allocated_height() * 0.25)
        
        self._v_scroll_value_animator.inter_step_delay = 1.0 / fps
        self._v_scroll_value_animator.step_val_change = self._v_scroll_adjustment.get_step_increment() / (fps * duration)
        self._v_scroll_value_animator.target_lower_bound = 0
        self._v_scroll_value_animator.target_upper_bound = self._v_scroll_adjustment.get_upper() - self._v_scroll_adjustment.get_page_size()
    
    def __getitem__(self, index):
        return self._items[index]
    
    def index_of(self, item):
        return self._items.index(item)
    
    @property
    def num_items(self):
        return len(self._items)

    def _adds_items(adder_fn):
        """ Any method that adds items should be decorated with this. """
        def wrapper(self, *args):
            self._new_item_was_added = True
            adder_fn(self, *args)
            self.rearrange_items_and_update_layout()
            self.scroll_to_top()
            self.show_all()
            return self
        return wrapper

    def _removes_items(rem_fn):
        """ Any method that removes items should be decorated with this. """
        def wrapper(self, *args):
            rem_fn(self, *args)
            self._update_layout()
            self.scroll_to_top()
            return self
        return wrapper
        
    def scroll_up(self):
        """ Moves the items downwards. """
        self._v_scroll_value_animator.target_val = self._v_scroll_value_animator.current_val - self._v_scroll_adjustment.get_step_increment()

    def scroll_down(self):
        """ Moves the items upwards. """
        self._v_scroll_value_animator.target_val = self._v_scroll_value_animator.current_val + self._v_scroll_adjustment.get_step_increment()

    def scroll_to_top(self):
        self._v_scroll_value_animator.target_val = self._v_scroll_value_animator.current_val = 0
        self._v_scroll_adjustment.set_value(0)

    @_removes_items
    def remove_item(self, item):
        self.remove(item)
        self._items.remove(item)
    
    @_removes_items
    def remove_all_items(self):
        for item in self._items:
            self.remove(item)
        self._items = []
    
    @_adds_items
    def add_items(self, items):
        for item in items:
            self.add(item)
            self._items.append(item)
    
    def rearrange_items_and_update_layout(self):
        """ Re-sorts the items in the widget. Call this if any items have been modified since they were
        added, just in case they need to be rearranged. """
        self._items.sort(key=self.sort_key_fn, reverse=self.sort_is_reversed)
        self._update_layout()
    
    def _update_layout(self):
        """ Assumes self._items to be sorted. Assumes all _items are already added to the layout. """
        pos_x = self.item_h_margin
        pos_y = self.item_v_margin
        for i in range(0, len(self._items)):
            item = self._items[i]
            self.move(item, pos_x, pos_y)
            pos_y += item.get_ideal_height() + self.item_v_margin
        self.set_size(self.get_size()[0], pos_y)
    
    def _on_resize(self, *_):
        width = self.get_allocated_width()
        height = self.get_allocated_height()
        if self._new_item_was_added or self._prev_width != width or self._prev_height != height:
            self._new_item_was_added = False
            self._prev_width = width
            self._prev_height = height
            if self.item_on_container_resized:
                for item in self._items:
                    self.item_on_container_resized(item, width, height)
            self._update_layout()

class WebBasedScrollableSortedFeedItemsBin(object):
    def __init__(self, app_settings = None, path_to_widget_theme_base_uri = None):
        self._app_settings = app_settings or tf_inject[DI.ClientAppSettings]
        self._path_to_widget_theme_base_uri = path_to_widget_theme_base_uri or tf_inject[DI.path_to_widget_theme_base_uri]

        self._sorted_feed_items = []

        self.category_setting_started_event = SimpleEvent()
        self.category_setting_finished_event = SimpleEvent()
        self.feed_item_selected_event = SimpleEvent()
        self.feed_view_window_user_activity_event = SimpleEvent()

        self._web_view_scroll_wrapper = Gtk.ScrolledWindow()
        self._override_gtk_widget_bg_to_transparent(self._web_view_scroll_wrapper)

        self._web_view = self._create_webkit_view_with_transparent_bg()
        self._web_view.connect("status-bar-text-changed", self._on_webkit_status_text_changed)
        self._web_view_scroll_wrapper.add(self._web_view)

        self._web_view_to_app_msg_trigger_keyword = 'msgToPython::'
        self._web_view_to_app_msg_parts_delim = '::'

        self._currently_open_feed_view_window = None

    @property
    def gtk_widget(self):
        return self._web_view_scroll_wrapper

    def _override_gtk_widget_bg_to_transparent(self, gtk_widget):
        all_state_flags = [Gtk.StateFlags.ACTIVE, Gtk.StateFlags.BACKDROP, Gtk.StateFlags.DIR_LTR, Gtk.StateFlags.DIR_RTL,
                           Gtk.StateFlags.FOCUSED, Gtk.StateFlags.INCONSISTENT, Gtk.StateFlags.INSENSITIVE, Gtk.StateFlags.NORMAL,
                           Gtk.StateFlags.PRELIGHT, Gtk.StateFlags.SELECTED]

        for flag in all_state_flags:
            gtk_widget.override_background_color(flag, Gdk.RGBA(222,222,222,0))

    def _create_webkit_view_with_transparent_bg(self):
        wv = WebKit.WebView()
        wv.set_transparent(True)
        self._override_gtk_widget_bg_to_transparent(wv)
        return wv

    def scroll_up(self):
        pass

    def scroll_down(self):
        pass

    def scroll_to_top(self):
        pass

    def rearrange_items_and_update_layout(self):
        pass

    def _feed_item_to_html_id(self, feed_item):
        return self._feed_item_idx_to_html_id(self._sorted_feed_items.index(feed_item))
    def _feed_item_idx_to_html_id(self, idx):
        return 'feed_item' + str(idx)
    def _feed_item_html_id_str_to_feed_item(self, html_id_str):
        return self._sorted_feed_items[int(html_id_str.strip('feed_item'))]

    def _display_feed_items(self):
        html = """ <html>
                <head>
                    <script type="text/javascript" src=jquery-2.0.3.js></script>

                    <style type="text/css">
                        .feed_item {
                            font-family: Helvetica;
                            font-size: 12px;
                            margin: 10px;
                            padding: 3px;

                            /*-moz-border-radius: 5px;
                            border-radius: 5px;
                            border-style: solid;
                            border-width: 2px;
                            border-color: gray;*/

                            box-shadow: 0px 0px 11px 0px #888888;

                            overflow : hidden;
                            text-overflow: ellipsis;
                            display: -webkit-box;
                            -webkit-line-clamp: 3;
                            -webkit-box-orient: vertical;

                            color: white;

                            -webkit-touch-callout: none;
                            -webkit-user-select: none;
                            user-select: none;
                            cursor: pointer;
                        }

                        .feed_item .title {
                            font-weight: bold;
                            margin-right: 10px;
                        }

                        .already_read {
                            color: rgb(150, 150, 150);
                        }

                        .feed_item .image img {
                            float: left;
                            margin-right: 5px;
                            max-height: 40px;
                            max-width: 60px;
                            /*border-radius: 5px;
                            border-style: solid;
                            border-width: 1px;
                            border-color: blue;*/
                        }
                    </style>

                    <script>
                        function onItemIsAlreadyRead_byId(item_id) {
                          var item = $("#" + item_id);
                          if (item.length) {
                            item.addClass("already_read");
                          }
                        }

                        // The only way a web_view can communicate with the gtk app: app listens to window.status changes.
                        // arguments is the parts of the message.
                        function msgToApp() {
                          var msgTriggerKeyword = '""" + self._web_view_to_app_msg_trigger_keyword + """';
                          var msgPartsDelim = '""" + self._web_view_to_app_msg_parts_delim + """';

                          window.status = msgTriggerKeyword + $.makeArray(arguments).join(msgPartsDelim);
                          window.status = "";    // Reset status for next msg, since the Webkit-Gtk API officially only sends events when window.status *changes*!
                        }

                        $(document).ready(function() {
                          $(".feed_item").click(function() {
                            msgToApp("item_clicked", this.id);
                          });
                        });
                    </script>
                </head>
                <body>""" + "".join([
                    """
                    <div id="{item_id}" class="{item_class}" style="background-color: rgba({bg_r},{bg_g},{bg_b}, {bg_a});">
                        {optional_image_html}
                        <span class="title">{title}</span>
                        <span class="description">{description}</span>
                    </div>
                    """.format(
                        item_id=self._feed_item_idx_to_html_id(idx),
                        item_class="feed_item" + (" already_read" if fi.is_read_by_user else ""),
                        optional_image_html= """<span class="image"><img src="{0}" /></span>""".format(fi.image_descriptor.url) if fi.image_descriptor else "",
                        title=fi.title.encode('utf-8'), # TODO: Shouldn't these be encoded at source?
                        description=fi.description.encode('utf-8'),
                        bg_r=int(math.ceil(fi.channel.color.r * 255)),
                        bg_g=int(math.ceil(fi.channel.color.g * 255)),
                        bg_b=int(math.ceil(fi.channel.color.b * 255)),
                        bg_a=self._app_settings.feed_widget_alpha
                    )
                for idx, fi in enumerate(self._sorted_feed_items)]) + \
            """</body>
            </html> """

        # For debugging only.
        # with open(os.path.expanduser('~/dumpedHtml.html'), 'w') as f: f.write(html)

        self._web_view.load_html_string(html, 'file://' + self._path_to_widget_theme_base_uri + '/')
        self._web_view.show_all()

    def show_category(self, feed_items_category):
        self.category_setting_started_event()

        this_request = object()
        self._newest_show_category_request = this_request

        def fetch_items_and_update_ui():
            items = feed_items_category.fetch_items()
            items.sort(key=feed_items_category.score_func, reverse=feed_items_category.sort_is_reversed)

            def update_ui(*_):
                if not self._newest_show_category_request is this_request:
                    return

                self._sorted_feed_items = items
                self._display_feed_items()
                self.category_setting_finished_event()
            run_in_ui_thread(update_ui)
        do_in_a_daemon_thread(fetch_items_and_update_ui)

    def _on_webkit_status_text_changed(self, _, new_txt):
        if not new_txt or not new_txt.startswith(self._web_view_to_app_msg_trigger_keyword):
            return
        msg_parts = new_txt.lstrip(self._web_view_to_app_msg_trigger_keyword).split(self._web_view_to_app_msg_parts_delim)
        if msg_parts[0] == 'print':
            print msg_parts[1]
        elif msg_parts[0] == 'item_clicked':
            self._on_feed_item_selected(self._feed_item_html_id_str_to_feed_item(msg_parts[1]))

    def _on_feed_item_selected(self, feed_item):
        self.feed_item_selected_event()
        feed_item.is_read_by_user = True
        self._web_view.execute_script('onItemIsAlreadyRead_byId("{0}");'.format(self._feed_item_to_html_id(feed_item)))

        # TODO: FeedItemsWindow should do this, just raise an event.
        self._create_and_show_feed_view_window_for_feed_widget(feed_item)

    def _create_and_show_feed_view_window_for_feed_widget(self, feed_item):
        def create_feed_view_window_dimensions():
            # TODO: The dimensions of the feeds window should not be taken from the settings.
            s = self._app_settings
            right = s.feeds_window_dimensions.left
            left = (right - s.feed_view_window_width) if s.feed_view_window_width else 50
            top = s.feed_view_window_top_y if s.feed_view_window_top_y else 0
            bottom = s.feed_view_window_bottom_y if s.feed_view_window_bottom_y else (Gdk.Screen.get_default().get_height() - 40)
            return BoxDimensions.by_right_and_bottom(left, top, right, bottom)

        if self._currently_open_feed_view_window:
            self._currently_open_feed_view_window.destroy()

        feed_view_window = FeedViewWindow(feed_item, create_feed_view_window_dimensions(),
                                          topfeed_social_features_are_enabled=False) # TODO: Remove the option completely.

        def on_feed_view_window_resized(*_):
            _, y = feed_view_window.get_position()
            s = self._app_settings
            s.feed_view_window_top_y = y
            s.feed_view_window_width = feed_view_window.get_allocated_width()
            s.feed_view_window_bottom_y = y + feed_view_window.get_allocated_height()
        feed_view_window.connect('size-allocate', on_feed_view_window_resized)

        feed_view_window.show_all()
        self._currently_open_feed_view_window = feed_view_window
        self._currently_open_feed_view_window.user_scroll_event += lambda: self.feed_view_window_user_activity_event()

class FeedItemsCategory(object):
    """ Encapsulates a category of feed items. Among other things, provides functions for obtaining the items in
        this category as well as a key function for sorting those widgets.
    """
    def __init__(self, feed_items_fetcher_func, feed_widget_score_func = None, sort_is_reveresed = False):
        self._items_fetcher_func = feed_items_fetcher_func
        self._item_score_func = feed_widget_score_func
        self.sort_is_reversed = sort_is_reveresed
        self.topfeed_social_features_are_enabled = True

    def fetch_items(self):
        """ This can be a long running task. """
        return self._items_fetcher_func()

    @property
    def score_func(self):
        return self._item_score_func


#noinspection PyMissingConstructor
class FeedWidgetsSorterScrollerAndFeedViewWindowManager(ItemsSorterScroller):
    """ A specialization of ItemSorterScroller, specifically for this app, and specifically for feed widgets as Items.
        Works with 'FeedWidgetsCategory's. Mainly, wires-up the various events of the widgets that are in the category.
    """
    def __init__(self, item_h_margin = 0, item_v_margin = 2, app_settings = None, server_session = None):
        self._super = super(FeedWidgetsSorterScrollerAndFeedViewWindowManager, self)
        self._super.__init__(None, None, item_h_margin, item_v_margin)

        self._app_settings = app_settings or tf_inject[DI.ClientAppSettings]
        self._server = server_session or tf_inject[DI.ServerSession]
        self._currently_open_feed_view_window = None

        self.category_setting_started_event = SimpleEvent()
        self.category_setting_finished_event = SimpleEvent()
        self.feed_widget_selected_event = SimpleEvent()
        self.feed_view_window_user_activity_event = SimpleEvent()

        self.category_setting_finished_event += self._disable_up_dn_btns_of_currently_open_fvw

    def show_category(self, widget_category):
        self.category_setting_started_event()

        this_request = object()
        self._newest_show_category_request = this_request

        def fetch_widgets_and_update_ui():
            widgets = widget_category.fetch_widgets

            def update_ui(*_):
                if not self._newest_show_category_request is this_request:
                    return

                for feed_widget in widgets:
                    feed_widget.connect('button-press-event', self._create_on_click_func_for(feed_widget, widget_category))

                self.remove_all_items()
                self.sort_key_fn = widget_category.score_func
                self.sort_is_reversed = widget_category.sort_is_reversed
                self._super.add_items(widgets)

                self.category_setting_finished_event()
            run_in_ui_thread(update_ui)
        do_in_a_daemon_thread(fetch_widgets_and_update_ui)

    def _create_on_click_func_for(self, feed_widget, widget_category):
        def _on_widget_click(*_):
            self._on_feed_widget_selected_for_reading(feed_widget, widget_category)
        return _on_widget_click
    
    def _on_feed_widget_selected_for_reading(self, feed_widget, widget_category):
        self.feed_widget_selected_event()
        if widget_category.topfeed_social_features_are_enabled and not feed_widget.feed.is_read_by_user:
            self._server.set_feed_as_read(feed_widget.feed.channel.link, feed_widget.feed.link)
            feed_widget.feed.is_read_by_user = True
            feed_widget.queue_draw()
            
        self._create_and_show_feed_view_window_for_feed_widget(feed_widget, widget_category)
    
    def _config_newly_created_feed_view_window(self, this_f_widget, this_f_view_window, this_widget_category):
        if self.index_of(this_f_widget) == 0:
            this_f_view_window.show_previous_item_btn.set_sensitive(False)
        if (self.num_items - 1) == self.index_of(this_f_widget):
            this_f_view_window.show_next_item_btn.set_sensitive(False)
        
        this_f_view_window.show_next_item_btn.connect('clicked',
            lambda *args: self._on_feed_widget_selected_for_reading(self[self.index_of(this_f_widget) + 1], this_widget_category))
        
        this_f_view_window.show_previous_item_btn.connect('clicked',
            lambda *args: self._on_feed_widget_selected_for_reading(self[self.index_of(this_f_widget) - 1], this_widget_category))

        def on_this_f_view_window_resized(*_):
            _, y = this_f_view_window.get_position()
            s = self._app_settings
            s.feed_view_window_top_y = y
            s.feed_view_window_width = this_f_view_window.get_allocated_width()
            s.feed_view_window_bottom_y = y + this_f_view_window.get_allocated_height()
        this_f_view_window.connect('size-allocate', on_this_f_view_window_resized)
    
    def _create_and_show_feed_view_window_for_feed_widget(self, feed_widget, widget_category):
        def create_feed_view_window_dimensions():
            s = self._app_settings
            right = s.feeds_window_dimensions.left
            left = (right - s.feed_view_window_width) if s.feed_view_window_width else 50
            top = s.feed_view_window_top_y if s.feed_view_window_top_y else 0
            bottom = s.feed_view_window_bottom_y if s.feed_view_window_bottom_y else (self.get_screen().get_height() - 40)
            return BoxDimensions.by_right_and_bottom(left, top, right, bottom)

        if self._currently_open_feed_view_window:
            self._currently_open_feed_view_window.destroy()

        feed_view_window = FeedViewWindow(feed_widget.feed,
                                          create_feed_view_window_dimensions(),
                                          topfeed_social_features_are_enabled=widget_category.topfeed_social_features_are_enabled)
        self._config_newly_created_feed_view_window(feed_widget, feed_view_window, widget_category)
        feed_view_window.show_all()
        self._currently_open_feed_view_window = feed_view_window
        self._currently_open_feed_view_window.user_scroll_event += lambda: self.feed_view_window_user_activity_event()

    def _disable_up_dn_btns_of_currently_open_fvw(self):
        if self._currently_open_feed_view_window:
            self._currently_open_feed_view_window.show_next_item_btn.set_sensitive(False)
            self._currently_open_feed_view_window.show_previous_item_btn.set_sensitive(False)


class AbstractFeedWidget(object):
    """
        In addition to supporting the methods of this class, a feed widget should also be a Gtk widget.
        Also a .feed field (or getter) is required, and used in the __eq__ method of this class.
    """
    def get_ideal_height(self): raise NotImplementedError()
    def on_container_resized(self, width, height): raise NotImplementedError()
    
    def __eq__(self, other):
        return self.feed == other.feed
    
    def __ne__(self, other):
        return not self.__eq__(other)


class FeedItemsWindow(Gtk.Window):
    def __init__(self, normal_feed_items_category, faved_feed_items_category, app_settings = None):
        super(FeedItemsWindow, self).__init__(Gtk.WindowType.TOPLEVEL, title=tf_inject[DI.app_name])

        if not normal_feed_items_category or not faved_feed_items_category:
            raise ValueError("Both 'FeedItemsCategory's must be provided.")

        self._last_user_activity_time_secs = 0

        self.normal_feed_items_category = normal_feed_items_category
        self.faved_feed_items_category = faved_feed_items_category

        self._app_settings = app_settings or tf_inject[DI.ClientAppSettings]
        
        self.set_visual(self.get_screen().get_rgba_visual())
        self.set_app_paintable(True)
        self.connect("draw", draw_cleared_bg)
        self.set_decorated(False)

        self.connect('delete-event', self._on_close_app_requested)
        self.connect('size-allocate', self._on_resized)
        
        my_dimensions = self._app_settings.feeds_window_dimensions
        self.set_default_size(my_dimensions.width, my_dimensions.height)
        self.move(my_dimensions.left, my_dimensions.top)
        
        self._main_container = Gtk.VBox()
        self.add(self._main_container)
        self._buttons_container = Gtk.HBox()
        self._main_container.pack_start(self._buttons_container, False, False, 4)
        self._button_up = Gtk.Button()
        self._button_up.add(Gtk.Image.new_from_stock(Gtk.STOCK_GO_UP, Gtk.IconSize.BUTTON))
        self._button_up.connect("button-press-event", self._on_button_up_pressed)
        self._button_up.connect("button-press-event", self._on_new_user_activity)
        self._button_dn = Gtk.Button()
        self._button_dn.add(Gtk.Image.new_from_stock(Gtk.STOCK_GO_DOWN, Gtk.IconSize.BUTTON))
        self._button_dn.connect("button-press-event", self._on_button_dn_pressed)
        self._button_dn.connect("button-press-event", self._on_new_user_activity)
        self._button_main_menu = Gtk.MenuButton()
        self._main_menu = Gtk.Menu()
        self._setup_main_menu()
        self._button_main_menu.set_popup(self._main_menu)
        self._button_main_menu.add(Gtk.Image.new_from_stock('topfeed_app_menu_icon', Gtk.IconSize.BUTTON))
        self._refresh_toggle_button = Gtk.ToggleButton()
        self._refresh_toggle_button.add(Gtk.Image.new_from_stock(Gtk.STOCK_REFRESH, Gtk.IconSize.BUTTON))
        self._refresh_toggle_button.connect('toggled', self._on_refresh_toggle_button_toggled_by_user)
        self._resize_toggle_button = Gtk.ToggleButton()
        self._resize_toggle_button.add(Gtk.Image.new_from_stock(Gtk.STOCK_FULLSCREEN, Gtk.IconSize.BUTTON))
        self._resize_toggle_button.set_tooltip_text('Resize & Move The Widget')
        self._resize_toggle_button.connect("toggled", self._on_button_resize_toggled)
        buttons_container_padding = 0
        self._buttons_container.pack_start(self._button_up, False, False, buttons_container_padding)
        self._buttons_container.pack_start(self._button_dn, False, False, buttons_container_padding)
        self._buttons_container.pack_start(self._refresh_toggle_button, False, False, buttons_container_padding)
        self._buttons_container.pack_start(self._resize_toggle_button, False, False, buttons_container_padding)
        self._buttons_container.pack_end(self._button_main_menu, False, False, buttons_container_padding)

        self._settings_dialog = None

        self._feeds_container = None
        self._init_feeds_container()

        self._current_refresh_future_job = None

    def _setup_main_menu(self):
        self._faves_mode_menu_item = Gtk.CheckMenuItem.new_with_label('Show Favorites')
        def on_faves_mode_menu_item_toggled(*_):
            if self._faves_mode_menu_item.get_active():
                self._set_and_show_feeds_category(self.faved_feed_items_category)
            else:
                self._set_and_show_feeds_category(self.normal_feed_items_category)
        self._faves_mode_menu_item.connect('toggled', on_faves_mode_menu_item_toggled)
        
        settings_menu_item = Gtk.ImageMenuItem.new_from_stock(Gtk.STOCK_PREFERENCES, None)
        settings_menu_item.set_always_show_image(True)

        def on_settings_menu_item_activated(*_):
            if self._settings_dialog: return    # Prevent multiple dialogs from being shown at once.

            self._settings_dialog = SettingsDialog()    # The SettingsDialog must be a field of self, else it may get GC'd (bug).

            minor_visual_settings_changed_handler = lambda: self.queue_draw()
            self._settings_dialog.widget_opacity_changed_event        += minor_visual_settings_changed_handler
            self._settings_dialog.channel_color_changed_event         += minor_visual_settings_changed_handler
            self._settings_dialog.use_text_shadows_flag_toggled_event += minor_visual_settings_changed_handler

            def update_layout_and_redraw():
                self._feeds_container._update_layout()  # TODO: Fix access to private field.
                self.queue_draw()
            self._settings_dialog.num_widget_lines_changed_event += update_layout_and_redraw
            self._settings_dialog.widget_font_set_event        += update_layout_and_redraw

            def rearrange_feeds_and_redraw():
                self._feeds_container.rearrange_items_and_update_layout()
                self.queue_draw()
            self._settings_dialog.channel_priority_changed_event += rearrange_feeds_and_redraw

            def on_settings_dialog_closed(*_):
                self._settings_dialog = None
            self._settings_dialog.window.connect('delete-event', on_settings_dialog_closed)

            self._settings_dialog.auto_refresh_delay_changed_event += lambda: self.reschedule_next_auto_refresh()

            self._settings_dialog.window.show_all()
        settings_menu_item.connect('activate', on_settings_menu_item_activated)

        user_guide_menu_item = Gtk.MenuItem.new_with_label("User Guide")
        def show_user_guide(*_):    # Can't simply use a lambda: Gtk will GC it prematurely.
            self._guide = StartersGuide()
            self._guide.show_all()
        user_guide_menu_item.connect('activate', show_user_guide)

        about_menu_item = Gtk.ImageMenuItem.new_from_stock(Gtk.STOCK_ABOUT, None)
        about_menu_item.set_always_show_image(True)
        about_menu_item.connect('activate', lambda *_: AboutTopfeedDialog().dialog.show_all())

        quit_menu_item = Gtk.ImageMenuItem.new_from_stock(Gtk.STOCK_QUIT, None)
        quit_menu_item.set_always_show_image(True)
        quit_menu_item.connect('activate', self._on_close_app_requested)
        
        self._main_menu.attach(self._faves_mode_menu_item, 0,1,0,1)
        self._main_menu.attach(settings_menu_item, 0,1,1,2)
        self._main_menu.attach(Gtk.SeparatorMenuItem(), 0,1,2,3)
        self._main_menu.attach(user_guide_menu_item, 0,1, 3,4)
        self._main_menu.attach(about_menu_item, 0,1,4,5)
        self._main_menu.attach(Gtk.SeparatorMenuItem(), 0,1, 5,6)
        self._main_menu.attach(quit_menu_item, 0,1, 6,7)
        self._main_menu.show_all()

        self._refresh_button_toggle_handler_is_disabled_for_once = True

    def _on_close_app_requested(self, *_):
        Gtk.main_quit()

    def show_all(self):
        super(FeedItemsWindow, self).show_all()
        self._set_and_show_feeds_category(self.normal_feed_items_category)

    def _set_and_show_feeds_category(self, feed_widgets_category):
        self._currently_shown_feed_widgets_category = feed_widgets_category
        self._feeds_container.show_category(feed_widgets_category)
        self.reschedule_next_auto_refresh()

    def _on_resized(self, *_):
        self._app_settings.feeds_window_dimensions = self.get_dimensions()
        self._app_settings.save_to_disk()   # TODO: This is a temporary fix for ensuring that settings are saved on abrupt system shutdowns.
    
    def _on_button_up_pressed(self, *_):
        if self._feeds_container:
            self._feeds_container.scroll_up()
    def _on_button_dn_pressed(self, *_):
        if self._feeds_container:
            self._feeds_container.scroll_down()
    
    def _on_button_resize_toggled(self, *_):
        self.set_decorated(True if self._resize_toggle_button.get_active() else False)

    def get_dimensions(self):
        x, y = self.get_position()
        return BoxDimensions.by_width_and_height(x, y, self.get_allocated_width(), self.get_allocated_height())

    def _on_refresh_toggle_button_toggled_by_user(self, *_):
        if self._refresh_button_toggle_handler_is_disabled_for_once:
            self._refresh_button_toggle_handler_is_disabled_for_once = False
            return

        self._set_and_show_feeds_category(self._currently_shown_feed_widgets_category)

    def programmatically_set_refresh_toggle_button_active(self, should_be_active):
        self._refresh_button_toggle_handler_is_disabled_for_once = True  # Disable the handler.
        self._refresh_toggle_button.set_active(should_be_active)

    def _init_feeds_container(self):
        self._feeds_container = WebBasedScrollableSortedFeedItemsBin()
        self._main_container.add(self._feeds_container.gtk_widget)

        self._feeds_container.category_setting_started_event  += lambda: self.programmatically_set_refresh_toggle_button_active(True)
        self._feeds_container.category_setting_finished_event += lambda: self.programmatically_set_refresh_toggle_button_active(False)
        self._feeds_container.feed_item_selected_event += self._on_new_user_activity
        self._feeds_container.feed_view_window_user_activity_event += self._on_new_user_activity

    def reschedule_next_auto_refresh(self):
        def reset_current_refresh_future_job_with_delay(job, delay_secs):
            if self._current_refresh_future_job:
                self._current_refresh_future_job.cancel()
            self._current_refresh_future_job = CancellableFutureJob(job, delay_secs)
            self._current_refresh_future_job.start()

        def create_self_delaying_refresh_job():
            def self_delaying_refresh_job():
                secs_since_last_activity = time.time() - self._last_user_activity_time_secs
                if secs_since_last_activity >= (self._app_settings.inter_auto_refresh_delay_secs - 5):  # The 5 is there just for safety.
                    self._set_and_show_feeds_category(self._currently_shown_feed_widgets_category)
                else:
                    reset_current_refresh_future_job_with_delay(create_self_delaying_refresh_job(),
                            self._app_settings.inter_auto_refresh_delay_secs - secs_since_last_activity)
            return self_delaying_refresh_job

        reset_current_refresh_future_job_with_delay(create_self_delaying_refresh_job(),
                self._app_settings.inter_auto_refresh_delay_secs)

    def _on_new_user_activity(self, *_):
        self._last_user_activity_time_secs = time.time()