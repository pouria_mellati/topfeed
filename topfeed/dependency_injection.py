"""
Created on May 13, 2013

@author: Pouria Mellati
"""
class FeatureBroker(object):
    """ Based on http://code.activestate.com/recipes/413268-dependency-injection-the-python-way/ """
    
    def __init__(self, allow_replace=False):
        self.providers = {}
        self.allow_replace = allow_replace
    
    def provide(self, feature, provider, *args, **kwargs):
        if not self.allow_replace:
            assert not self.providers.has_key(feature), "Duplicate feature: %r" % feature
        if callable(provider):
            def call(): return provider(*args, **kwargs)
        else:
            def call(): return provider
        self.providers[feature] = call
    
    def unbind_all(self):
        self.providers = {}
        
    def __getitem__(self, feature):
        try:
            provider = self.providers[feature]
        except KeyError:
            raise KeyError, "Unknown feature named %r" % feature
        return provider()

class DI(object):
    """ Enumeration of injectables. A better but longer name for this would be Injectables. """
    
    ClientAppSettings = "ClientAppSettings"
    ServerUserSettings = "ServerUserSettings"
    FeedsFetcher = "FeedsFetcher"
    resources_dir = "resources_dir"
    server_url_http = "server_url_http"
    server_url_https = "server_url_https"
    Server = "Server"
    ServerSession = "ServerSession"
    ui_descriptions_dir = "ui_descriptions_dir"
    app_home = "app_home"
    webkit_cookies_file_path = "webkit_cookies_file_path"
    app_tmp_dir = "app_tmp_dir"
    app_image_cache_dir = "app_image_cache_dir"
    app_name = 'app_name'
    app_version = 'app_version'
    path_to_start_script = 'path_to_start_script'
    path_to_widget_theme_base_uri = 'path_to_widget_theme_base_uri'
