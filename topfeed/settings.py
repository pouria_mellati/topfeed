'''
Created on Jan 20, 2013

@author: Pouria Mellati
'''
from os import path
from gui.utils import BoxDimensions
from topfeed.utils import ensure_directory_exists, KeyedEqualityNHashMixin
from topfeed.dialogs import LoginDialoge
import os, pickle

class UserSettings(KeyedEqualityNHashMixin):
    ''' The user's topfeed settings, regardless of the topfeed client app. Can be asked for from the server. '''
    def __init__(self, channels = set()):
        self.channels = channels
    
    def __key__(self):
        return self.channels

# The base class of all exceptions classes in this module.
class ClientSettingsException(Exception): pass
class CoudNotLoadClientSettingsException(ClientSettingsException): pass # TODO: Rename to match with the saving Error below.
class SavingClientSettingsFailedException(ClientSettingsException): pass

class ClientAppSettings(object): pass   # Used for dependency injection.

# TODO: Introduce getters and setters for fields. The setters should fire events that users of settings can connect to.
class RealClientAppSettings(ClientAppSettings):
    """ Note:  The users from outside this module should not use get() or __init__().
    Instead they are supposed to inject a ClientAppSettings using dependency injection.
    """
    @staticmethod
    def get(settings_file_host_dir):
        """ Returns a tuple, whose first element is the retrieved ClientAppSettings, and the second element is whether the
            user had to log-in first.
        """
        if not settings_file_host_dir:
            raise ValueError("A settings_file_host_dir should be provided.")

        user_had_to_login = False
        ensure_directory_exists(settings_file_host_dir)
        path_to_settings_file = path.join(settings_file_host_dir, 'settings')
        if not path.isfile(path_to_settings_file):
            session_id = LoginDialoge().run()
            RealClientAppSettings(path_to_settings_file, session_id).save_to_disk()
            user_had_to_login = True
        return RealClientAppSettings.get_client_settings_from_disk(path_to_settings_file), user_had_to_login
    
    @staticmethod
    def get_client_settings_from_disk(path_to_settings_file):
        try:
            with open(path_to_settings_file, 'r') as f:
                # The set of attributes of this class changes as the source changes in development.
                # Below, it is ensured that the returned settings always conforms to the current source.
                loaded = pickle.load(f)
                evolved_settings = RealClientAppSettings(path_to_settings_file, "")
                for attrib in evolved_settings.__dict__.keys():
                    setattr(evolved_settings, attrib, getattr( loaded, attrib, (getattr(evolved_settings, attrib)) ))
                return evolved_settings
        except:
            raise CoudNotLoadClientSettingsException
    
    def __init__(self, path_to_settings_file, session_id):
        self.session_id = session_id
        self.path_to_settings_file = path_to_settings_file
        self.feeds_window_dimensions = self._create_initial_feeds_window_dimensions()
        self.feed_widget_alpha = 0.7    # TODO: Rename to widget_alpha.
        self.widget_text_max_num_lines = 3
        self.widget_text_shadow_enabled = True
        self.widget_font_desc_str = "Sans 9"

        self.feed_view_window_width = None
        self.feed_view_window_top_y = None
        self.feed_view_window_bottom_y = None

        self.inter_auto_refresh_delay_secs = 30 * 60

    @staticmethod
    def _create_initial_feeds_window_dimensions():
        from gi.repository import Gdk
        right = Gdk.Screen.width()
        bottom = Gdk.Screen.height()
        width = 300
        return BoxDimensions.by_right_and_bottom(right - width, 0, right, bottom)

    def save_to_disk(self):
        path_to_tmp_settings_file = self.path_to_settings_file + '.tmp'
        try: 
            with open(path_to_tmp_settings_file, 'w+') as tmpSettingsFile:
                pickle.dump(self, tmpSettingsFile, 2)
        except:
            raise SavingClientSettingsFailedException()
        else:
            if path.isfile(self.path_to_settings_file):
                os.remove(self.path_to_settings_file)
            os.rename(path_to_tmp_settings_file, self.path_to_settings_file)