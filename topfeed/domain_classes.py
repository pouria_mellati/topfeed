'''
Contains domain model objects.

Created on Feb 16, 2013

@author: Pouria Mellati
'''
from topfeed.gui.utils import Color
from datetime import datetime
from topfeed.utils import KeyedEqualityNHashMixin
import logging

class Channel(KeyedEqualityNHashMixin):
    def __init__(self, link, color = Color(0,0,1,.8), priority = 0.5):
        if (not link):
            raise ValueError("Channel link cannot be empty.")
        self.link = link
        self.color = color
        self.priority = priority
    
    def __key__(self):
        return (self.link)

class FeedCreationException(Exception):pass

class Feed(object):
    """ In this project, a Feed actually represents a single feed item. What one would otherwise call
        a feed, is named a Channel here.
    """
    # TODO: Fix the id creation in this class, just use feed link as id. (Remove id field.)
    def __init__(self, item_id, channel, link, **args):
        if not item_id:
            if args.get('title') and link:
                logging.warn("Feed with title " + args.get('title') + ", to be instantiated, has no id, using title+link as id.")
                item_id = args.get('title') + link
            else:
                raise FeedCreationException("Cannot create an id with the parameters provided.")
        if not channel or not link:
            raise FeedCreationException("Both channel & link params must be supplied.")
        
        self.id = item_id
        self.channel = channel
        self.link = link
        self.title = args.get('title', 'Untitled')
        self.description = args.get('description', '')
        self.image_descriptor = args.get('image_descriptor')
        self.pub_date = args.get('pub_date')
        self.client_timestamp = args.get('client_timestamp', datetime.now())
        self.num_upvotes = args.get('num_upvotes', 0)
        self.num_dnvotes = args.get('num_dnvotes', 0)
        self.num_reads = args.get('num_reads', 0)
        self.is_read_by_user = args.get('is_read_by_user', False)
        self.is_favorite = args.get('is_favorite', False)
        self.user_vote = args.get('user_vote', 0)
        self._assert_is_valid_user_vote_value(self.user_vote)
        
        
    def __eq__(self, other):
        return True if type(self) is type(other) and self.id == other.id else False
    def __ne__(self, other):
        return not self.__eq__(other)
    
    @property
    def votes_balance(self):
        return self.num_upvotes - self.num_dnvotes
    
    def set_user_vote(self, new_vote):
        self._assert_is_valid_user_vote_value(new_vote)
        old_vote = self.user_vote
        if(new_vote == old_vote):
            return
        
        if(new_vote == 1):
            self.num_upvotes += 1      
        elif(new_vote == -1):
            self.num_dnvotes += 1
            
        if(old_vote == 1):
            self.num_upvotes -= 1
        elif(old_vote == -1):
            self.num_dnvotes -= 1
        
        self.user_vote = new_vote
    
    def _assert_is_valid_user_vote_value(self, user_vote_val):
        if(user_vote_val > 1 or user_vote_val < -1):
            raise ValueError("The user_vote value must be one of -1, 0, 1.")

class FavoriteFeedInfo(object):
    def __init__(self, url, channel_url, title, description, date_time):
        self.url = url
        self.channel_url = channel_url
        self.title = title
        self.description = description
        self.date_time = date_time

class ServerFeedInfo(object):
    """ Information on a feed retrieved from the Topfeed server. """
    def __init__(self, link, num_upvotes, num_dnvotes, num_reads, user_vote, is_read_by_user):
        self.link = link
        self.num_upvotes = num_upvotes
        self.num_dnvotes = num_dnvotes
        self.num_reads = num_reads
        self.user_vote = user_vote    # +1: Up-vote, 0: No Vote, -1: Down-vote
        self.is_read_by_user = is_read_by_user