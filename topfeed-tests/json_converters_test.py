'''
Created on Jul 25, 2013

@author: Pouria Mellati
'''
import unittest, json
from topfeed.json_converters import json_str_2_UserSettings
from topfeed.domain_classes import Channel
from topfeed.settings import UserSettings
from topfeed.gui.utils import Color

class Test(unittest.TestCase):
    def test_json_2_UserSettings(self):
        ch1 = Channel('http://url1', Color( 1,   0, .3, 1))
        ch2 = Channel('http://url2', Color(.1, 0.7,  0, 1))
        userSettings = UserSettings(channels = {ch1, ch2})
        
        settings_json_str = json.dumps({
            'channels': [
                {
                    'url': ch1.link,
                    'color': {
                        'r': ch1.color.r,
                        'g': ch1.color.g,
                        'b': ch1.color.b
                    }
                 },
                 {
                    'url': ch2.link,
                    'color': {
                        'r': ch2.color.r,
                        'g': ch2.color.g,
                        'b': ch2.color.b
                    }
                 }
            ]
        })
        
        self.assertEquals(json_str_2_UserSettings(settings_json_str), userSettings)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()